//
//  DropDownButton.h
//  pickerControl
//
//  Created by Aisha on 5/27/13.
//  Copyright (c) 2013 Aisha. All rights reserved.
//

#import <UIKit/UIKit.h>
NSMutableArray *dropDownViewArray;
@protocol dropDownProtocol <NSObject>
-(void) indexClickedForDropDownMenuIndex:(int) index andDropDown:(id)dropDown;
@end
@interface DropDownButton : UIButton<UITableViewDataSource,UITableViewDelegate>
{
    UITableView *tableView;
}

-(void) initButtonWithData:(NSMutableArray *)array parentTarget:(id)parents toView:(UIView *) parentView cellBackground:(UIImage*) cellBackground;
-(void) dismissControlWhenClickedOutside;
@property(nonatomic,assign) int isOpen;
@property(nonatomic,retain) NSMutableArray *data;
@property(nonatomic,retain) id parent;
+(void) dismissButtons:(NSMutableArray*) buttonsArray callingButton:(id) sender;
@property(nonatomic,assign) int mode;


@end
