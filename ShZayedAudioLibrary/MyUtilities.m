//
//  MyUtilities.m
//  Yomiyat
//
//  Created by Aisha on 5/19/13.
//  Copyright (c) 2013 Aisha. All rights reserved.
//

#import "MyUtilities.h"
#import "DropDownButton.h"

@implementation MyUtilities
#pragma Mark READING HTML

+(void) readHTMLFileName:(NSString*) fileName onView:(UIWebView*)webView;
{
    NSURL *url = [[NSBundle mainBundle] URLForResource:fileName withExtension:@"html"];
    NSString *html = [NSString stringWithContentsOfURL:url encoding:NSUTF8StringEncoding error:nil];
    NSURL *baseUrl = [NSURL fileURLWithPath:[[NSBundle mainBundle] bundlePath]];
    [webView loadHTMLString:html baseURL:baseUrl];
}

+(void) removeshadowLineOnWebView:(UIWebView*)webView;
{
    //remove shadow
    for(UIView *wview in [[[webView subviews] objectAtIndex:0] subviews]) {
        if([wview isKindOfClass:[UIImageView class]]) { wview.hidden = YES; }
    }
}

#pragma Mark UICollectionView

//Add a folder MyUICollectionView (Drag and drop to any folder ). Make UICollection with IBOutlet and connect and call below function for localData

/*-(void)setLocalDataForgalleryCollectionView:(UICollectionView*) collectionViewC callingClass:(id) selfClass
{
    collectionViewData =[[CollectionViewData alloc] init];
    [collectionViewC registerNib:[UINib nibWithNibName:@"MyCell" bundle:[NSBundle mainBundle]] forCellWithReuseIdentifier:@"MY_IDENTIFIER"];
    [collectionViewC registerNib:[UINib nibWithNibName:@"MyAlbumCell" bundle:[NSBundle mainBundle]] forCellWithReuseIdentifier:@"MY_IDENTIFIER_ALBUM"];
    collectionViewC.delegate=collectionViewData;
    collectionViewC.dataSource=collectionViewData;
    collectionViewData.caller=selfClass;
}*/
#pragma For Youmyat

+(void) setSourceImagesOnForString:(NSString *) sourceName OnView:(UIImageView*) sourceImageView
{
    if([sourceName isEqualToString:@"(الاتحاد)"])
    {
        sourceImageView.image=[UIImage imageNamed:@"الاتحاد.png"];
    }
    else if([sourceName isEqualToString:@"(البيان)"])
    {
        sourceImageView.image=[UIImage imageNamed:@"البيان.png"];
    }
    else if([sourceName isEqualToString:@"(الخليج)"])
    {
        sourceImageView.image=[UIImage imageNamed:@"الخليج.png"];
    }
    else if([sourceName isEqualToString:@"(الوحدة)"])
    {
        sourceImageView.image=[UIImage imageNamed:@"الوحدة.png"];
    }
    else if([sourceName isEqualToString:@"(نشرة الرصد)"])
    {
        sourceImageView.image=[UIImage imageNamed:@"نشرة الرصد.png"];
    }

    sourceImageView.contentMode=UIViewContentModeScaleAspectFit;
}




#pragma mark

@end
