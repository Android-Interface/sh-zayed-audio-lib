//
//  BrowseViewController.h
//  Yomiyat
//
//  Created by Aisha on 5/20/13.
//  Copyright (c) 2013 Aisha. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DropDownButton.h"
#import "Common.h"

#import "PhotoArchivesDataManager.h"
#import "AudioArchive.h"
#import "LanguageManager.h"
#import <StoreKit/StoreKit.h>

@class GalleryRssParser;
@class GalleryRss;
@interface BrowseViewController : UIViewController<dropDownProtocol,UITextFieldDelegate,AVAudioPlayerDelegate,SKStoreProductViewControllerDelegate>
{
    PhotoArchivesDataManager *archivesDB;
    
    IBOutlet UIActivityIndicatorView *activityIndicator;
    IBOutlet UITableView *browseTableView;
    NSMutableArray *searchArray;
    
    IBOutlet UIImageView *homeImageView;
    IBOutlet UINavigationBar *navBar;
    IBOutlet UIImageView *topTitleImageView;
    
    IBOutlet UIButton *helpScreen;
    IBOutlet UIView *mpVolumeViewParentView;
    
    IBOutlet UITextField *searchTextField;
    IBOutlet DropDownButton *placeDropDownButton;
    IBOutlet DropDownButton *categoryDropDownButton;
    IBOutlet DropDownButton *fromYearDropDownButton;
    IBOutlet DropDownButton *toYearDropDownButton;
    __weak IBOutlet UIButton *playAllButton;
    
    IBOutlet UISlider *playerSlider;
    NSMutableArray *placesDropdownArray;
    NSMutableArray *categoriesDropDownArray;
    NSMutableArray *datesDropDownArray;
    
    IBOutlet UIButton *playPauseButton;
    int selectedIndexPath;
    //
    LanguageManager *languageSharedManager;
    
    IBOutlet UILabel *titleLabel;
    int fontSize;
    
    int currentIndex;
    
    IBOutlet UIImageView *tableBackgroundView;
    
    IBOutlet UIImageView *searchBackgroundImageView;
    IBOutlet UIButton *searchButton;
    
    
    AVAudioPlayer *audioPlayer;
    BOOL isPause;
    BOOL playAll;
}
- (IBAction)advancedButtonPressed:(id)sender;
- (IBAction)slide ;
-(IBAction) goBackToList:(id)sender;
-(IBAction)goNextPage:(id)sender;
-(IBAction)goPrePage:(id)sender;
-(IBAction)OpenHelp:(id)sender;
-(void)dismissHelpFromView;

@end