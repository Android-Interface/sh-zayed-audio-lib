//
//  MoreApp.m
//  ShZayedAudioLibrary
//
//  Created by Anam on 2/12/14.
//  Copyright (c) 2014 Interface. All rights reserved.
//

#import "MoreApp.h"

@implementation MoreApp
@synthesize appID,appIcon,appLink,appDescriptionArabic,appDescriptionEnglish,appNameArabic,appNameEnglish;

- (id)init
{
    self = [super init];
    if (self)
    {
        // Initialization code here.
       
        appLink=[[NSString alloc]init];
        appIcon=[[NSString alloc]init];
        appNameEnglish=[[NSString alloc]init];
        appNameArabic=[[NSString alloc]init];
        appDescriptionEnglish=[[NSString alloc]init];
        appDescriptionArabic=[[NSString alloc]init];
        
    }
    
    return self;
}

@end
