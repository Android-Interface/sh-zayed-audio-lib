//
//  CustomCell.m
//  Yomiyat
//
//  Created by Aisha on 5/14/13.
//  Copyright (c) 2013 Aisha. All rights reserved.
//

#import "CustomCellMore.h"

@implementation CustomCellMore

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)layoutSubviews {
    [super layoutSubviews];
    UIDevice* thisDevice = [UIDevice currentDevice];
    if(thisDevice.userInterfaceIdiom == UIUserInterfaceIdiomPad)
    {
        // iPad
        if (languageChange==0) {
            self.textLabel.frame = CGRectMake(140, 10, self.contentView.frame.size.width-30, 40);
            self.detailTextLabel.frame = CGRectMake(140, 10, self.contentView.frame.size.width-185, 130);

            self.imageView.frame=CGRectMake(20.0, 20.0, 110.0, 110.0);
        } else {
            self.textLabel.frame = CGRectMake(30, 10, self.contentView.frame.size.width-170, 40);
            self.detailTextLabel.frame = CGRectMake(30,10, self.contentView.frame.size.width-170, 130);
            self.imageView.frame=CGRectMake(self.contentView.frame.size.width-130, 20.0, 110.0, 110.0);
        self.textLabel.textColor=[UIColor blackColor];
        self.detailTextLabel.textColor=[UIColor blackColor];
        }
           }
    else
    {
        // iPhone
       
        self.textLabel.textColor=[UIColor blackColor];
        self.detailTextLabel.textColor=[UIColor blackColor];
        if (languageChange==0) {
            self.textLabel.frame = CGRectMake(60.0, 7.5, self.contentView.frame.size.width-65, 60);
            self.detailTextLabel.frame = CGRectMake(55.0, 60, self.contentView.frame.size.width-65, 130);
            
            self.imageView.frame=CGRectMake(10.0, 10.0, 45.0, 45.0);
        } else {
            self.textLabel.frame = CGRectMake(30, 7.5, self.contentView.frame.size.width-90, 60.0);
            self.detailTextLabel.frame = CGRectMake(30, self.detailTextLabel.frame.origin.y, self.contentView.frame.size.width-90, self.textLabel.frame.size.height);
            self.imageView.frame=CGRectMake(self.contentView.frame.size.width-50, 10.0, 45.0, 45.0);
            self.textLabel.textColor=[UIColor blackColor];
            self.detailTextLabel.textColor=[UIColor blackColor];
        }
    }
  
    if (IS_IPAD) {
        self.textLabel.font=[UIFont boldSystemFontOfSize:18.0];// fontWithName:@"Ariel" size:11];
        self.detailTextLabel.font=[UIFont systemFontOfSize:16.0];// fontWithName:@"Ariel" size:11];
        
    }
    else{
        //self.textLabel.font=[UIFont boldSystemFontOfSize:18.0];// fontWithName:@"Ariel" size:11];
        self.textLabel.font=[UIFont systemFontOfSize:14.0];// fontWithName:@"Ariel" size:11];
        
    }
    // self.textLabel.frame.origin.y
}
@end
