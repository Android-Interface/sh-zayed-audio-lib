//
//  CustomCell.m
//  Yomiyat
//
//  Created by Aisha on 5/14/13.
//  Copyright (c) 2013 Aisha. All rights reserved.
//

#import "CustomCell.h"

@implementation CustomCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)layoutSubviews {
    [super layoutSubviews];
    UIDevice* thisDevice = [UIDevice currentDevice];
    if(thisDevice.userInterfaceIdiom == UIUserInterfaceIdiomPad)
    {
        // iPad
//        if (languageChange==0) {
//            self.textLabel.frame = CGRectMake(80, 0, self.contentView.frame.size.width-10, self.textLabel.frame.size.height);
//            self.detailTextLabel.frame = CGRectMake(80, self.detailTextLabel.frame.origin.y, self.contentView.frame.size.width-10, self.textLabel.frame.size.height);
//
//            self.imageView.frame=CGRectMake(10.0, 10.0, 60.0, 60.0);
//        } else {
            self.textLabel.frame = CGRectMake(50, 10, self.contentView.frame.size.width-130, self.textLabel.frame.size.height);
            self.detailTextLabel.frame = CGRectMake(50, self.detailTextLabel.frame.origin.y, self.contentView.frame.size.width-130, self.textLabel.frame.size.height);
            self.imageView.frame=CGRectMake(self.contentView.frame.size.width-70, 10.0, 60.0, 60.0);
        self.textLabel.textColor=[UIColor whiteColor];
        self.detailTextLabel.textColor=[UIColor whiteColor];
        //}
           }
    else
    {
        // iPhone
        self.textLabel.frame = CGRectMake(30, 7.5, self.contentView.frame.size.width-90, self.textLabel.frame.size.height);
        self.detailTextLabel.frame = CGRectMake(30, self.detailTextLabel.frame.origin.y, self.contentView.frame.size.width-90, self.textLabel.frame.size.height);
        self.imageView.frame=CGRectMake(self.contentView.frame.size.width-50, 7.5, 45.0, 45.0);
        self.textLabel.textColor=[UIColor blackColor];
        self.detailTextLabel.textColor=[UIColor blackColor];
    }
  
    if (IS_IPAD) {
        self.textLabel.font=[UIFont fontWithName:@"Ariel" size:11];
        self.detailTextLabel.font=[UIFont fontWithName:@"Ariel" size:11];
        
    }
    else{
        self.textLabel.font=[UIFont fontWithName:@"Arial" size:14];
        self.detailTextLabel.font=[UIFont fontWithName:@"Arial" size:14];
        
    }
    self.imageView.contentMode = UIViewContentModeScaleAspectFit;

    // self.textLabel.frame.origin.y
}
@end
