//
//  MoreApp.h
//  ShZayedAudioLibrary
//
//  Created by Anam on 2/12/14.
//  Copyright (c) 2014 Interface. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MoreApp : NSObject
{
    int appID;
    NSString *appIcon;
    NSString *appLink;
   
    NSString *appNameEnglish;
    NSString *appDescriptionEnglish;
    
    NSString *appNameArabic;
    NSString *appDescriptionArabic;
   


}
@property(nonatomic,assign)int appID;
@property(nonatomic,retain)NSString *appLink;
@property(nonatomic,retain)NSString *appIcon;
@property(nonatomic,retain)NSString *appNameEnglish;
@property(nonatomic,retain)NSString *appDescriptionEnglish;
@property(nonatomic,retain)NSString *appNameArabic;
@property(nonatomic,retain)NSString *appDescriptionArabic;


@end
