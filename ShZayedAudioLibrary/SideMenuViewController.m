//
//  SideMenuViewController.m
//  MFSideMenuDemo
//
//  Created by Michael Frederick on 3/19/12.

//
//  SideMenuViewController.m
//  MFSideMenuDemo
//
//  Created by Michael Frederick on 3/19/12.

#import "SideMenuViewController.h"
#import "MFSideMenu.h"


//#import "DesignViewController.h"
//#import "ContactUsViewController.h"
//#import "RssFunViewController.h"
//#import "SocialDetailViewController.h"
//#import "SDWebImageRootViewController.h"
//#import "ContactUsViewController.h"

@implementation SideMenuViewController


- (id)init
{
    self = [super init];
    if (self) {
        NSLog(@"nslog");
       // self.view.backgroundColor=[UIColor whiteColor];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.tableView.backgroundColor=[UIColor whiteColor];
    self.navigationController.navigationBarHidden=NO;
    // self.tableView.frame=CGRectMake(0,100,320,self.view.frame.size.height);
    arrayOfDocuments = [[NSArray alloc] initWithObjects:
                        @"interface_brochure_iPad.pdf", nil];
    self.tableView.autoresizingMask=UIViewAutoresizingFlexibleBottomMargin|UIViewAutoresizingFlexibleTopMargin |UIViewAutoresizingFlexibleHeight |UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleBottomMargin|UIViewAutoresizingFlexibleBottomMargin;
}

#pragma mark - UITableViewDataSource

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return @"";//[NSString stringWithFormat:@"Section %d", section];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 6;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    cell.textLabel.text = [NSString stringWithFormat:@"Item %d", indexPath.row];
    cell.textLabel.textColor=[UIColor darkTextColor];
    
    cell.backgroundView =  [[UIImageView alloc] initWithImage:[ [UIImage imageNamed:@"main-Color.png"] stretchableImageWithLeftCapWidth:0.0 topCapHeight:5.0] ];
    cell.contentView.backgroundColor=[UIColor clearColor];
    cell.backgroundColor=[UIColor clearColor];
    
    UIView *selectedView=[[UIView alloc] initWithFrame:CGRectMake(0,0,320,60) ];
    selectedView.backgroundColor=[UIColor lightGrayColor];
    cell.selectedBackgroundView =selectedView;
    
    
    UIView *cellView=[[UIView alloc] initWithFrame:CGRectMake(0,0,320,60 )];
    cellView.backgroundColor=[UIColor darkGrayColor];
    cell.backgroundView=cellView;
    //[ [[UIImageView alloc] initWithImage:[ [UIImage imageNamed:@"main-delimiter.png"] stretchableImageWithLeftCapWidth:0.0 topCapHeight:5.0] ]autorelease];
    cell.textLabel.textColor=[UIColor whiteColor];
    
    switch (indexPath.row)
    {
//        case 0:
//            cell.textLabel.text = @"Home";
//            break;
//        case 1:
//            cell.textLabel.text = @"Interface Profile";
//            break;
        case 0:
            cell.textLabel.text = @"Home";
             cell.imageView.image=[UIImage imageNamed:@"home.png"];
            break;
        case 1:
            cell.textLabel.text = @"Facebook";
            cell.imageView.image=[UIImage imageNamed:@"facebook.png"];
            break;
            
        case 2:
            cell.textLabel.text = @"Twitter";
             cell.imageView.image=[UIImage imageNamed:@"twitter.png"];
            break;
            
        case 3:
            cell.textLabel.text = @"Gallery";
             cell.imageView.image=[UIImage imageNamed:@"gallery.png"];
            break;
            
        case 4:
            cell.textLabel.text = @"Contact Us";
              cell.imageView.image=[UIImage imageNamed:@"Contact us.png"];
            break;
            
        case 5:
            cell.textLabel.text = @"Location";
              cell.imageView.image=[UIImage imageNamed:@"location.png"];
            break;

            
        default:
            break;
    }
    
    return cell;
}
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell.backgroundColor = [UIColor clearColor];
}
-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}
#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
  /*   if(indexPath.row==3)
    {
        SDWebImageRootViewController *sponsor=[[SDWebImageRootViewController alloc] init];//WithNibName:@"SDWebImageRootViewController" bundle:[NSBundle mainBundle]] autorelease];
        
        //reset home tab bar item
        //sponsor.title = @"Home";
        sponsor.tabBarItem.image=[UIImage imageNamed:@"home.png"];
        
        NSArray *controllers = [NSArray arrayWithObject:sponsor];
        self.sideMenu.navigationController.viewControllers = controllers;
        [self.sideMenu setMenuState:MFSideMenuStateHidden];
    }
   
    else if(indexPath.row==2)
    {
        SocialDetailViewController *social=[[[SocialDetailViewController alloc] initWithNibName:@"SocialDetailViewController" bundle:[NSBundle mainBundle]] autorelease];
        social.urlString=@"https://twitter.com/interface_uae";
        social.titleMode=0;
        //social.title = @"Home";
        social.tabBarItem.image=[UIImage imageNamed:@"home.png"];
        
        NSArray *controllers = [NSArray arrayWithObject:social];
        self.sideMenu.navigationController.viewControllers = controllers;
        [self.sideMenu setMenuState:MFSideMenuStateHidden];
    }
    else if(indexPath.row==1)
    {
        SocialDetailViewController *social=[[[SocialDetailViewController alloc] initWithNibName:@"SocialDetailViewController" bundle:[NSBundle mainBundle]] autorelease];
        social.urlString=@"https://www.facebook.com/interface.ae";//https://www.facebook.com/interface.jlt
        social.titleMode=1;
        //reset home tab bar item
       // social.title = @"Home";
        social.tabBarItem.image=[UIImage imageNamed:@"home.png"];
        
        NSArray *controllers = [NSArray arrayWithObject:social];
        self.sideMenu.navigationController.viewControllers = controllers;
        [self.sideMenu setMenuState:MFSideMenuStateHidden];
    }
    
    else if(indexPath.row==0)
    {
        HomeViewController *social=[[[HomeViewController alloc] initWithNibName:@"HomeViewController" bundle:[NSBundle mainBundle]] autorelease];
       //        social.title = @"Home";
        social.tabBarItem.image=[UIImage imageNamed:@"home.png"];
        
        NSArray *controllers = [NSArray arrayWithObject:social];
        self.sideMenu.navigationController.viewControllers = controllers;
        [self.sideMenu setMenuState:MFSideMenuStateHidden];
    }
    else if(indexPath.row==4)
    {
        ContactUsViewController *social=[[[ContactUsViewController alloc] initWithNibName:@"ContactUsViewController" bundle:[NSBundle mainBundle]] autorelease];
       // social.title = @"Home";
        social.tabBarItem.image=[UIImage imageNamed:@"home.png"];
        
        NSArray *controllers = [NSArray arrayWithObject:social];
        self.sideMenu.navigationController.viewControllers = controllers;
        [self.sideMenu setMenuState:MFSideMenuStateHidden];
    }
    else if(indexPath.row==5)
    {
        SocialDetailViewController *social=[[[SocialDetailViewController alloc] initWithNibName:@"SocialDetailViewController" bundle:[NSBundle mainBundle]] autorelease];
      
        //25.069186,55.139383
        NSString *latlong = @"25.069186,55.139383";
        NSString *url = [NSString stringWithFormat: @"http://maps.google.com/maps?ll=%@",
                         [latlong stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
        //
        social.urlString=url;//@"https://www.facebook.com/interface.jlt";
        social.titleMode=4;
        
       // social.title = @"Home";
        social.tabBarItem.image=[UIImage imageNamed:@"home.png"];
        
        NSArray *controllers = [NSArray arrayWithObject:social];
        self.sideMenu.navigationController.viewControllers = controllers;
        [self.sideMenu setMenuState:MFSideMenuStateHidden];
    }*/
}



- (NSInteger) numberOfPreviewItemsInPreviewController: (QLPreviewController *) controller
{
    return [arrayOfDocuments count];
}

- (id <QLPreviewItem>)previewController: (QLPreviewController *)controller previewItemAtIndex:(NSInteger)index
{
    // Break the path into its components (filename and extension)
    //  NSArray *fileComponents = [[arrayOfDocuments objectAtIndex: index] componentsSeparatedByString:@"."];
    // Use the filename (index 0) and the extension (index 1) to get path
    //  NSString *path = [[NSBundle mainBundle] pathForResource:[fileComponents objectAtIndex:0] ofType:[fileComponents objectAtIndex:1]];
    NSString *path = [[NSBundle mainBundle] pathForResource:@"interface_brochure_iPad" ofType:@"pdf"];
    return [NSURL fileURLWithPath:path];
}

//-(void)dealloc
//{
//    [arrayOfDocuments release];
//    [super dealloc];
//}
@end


/*#import "SideMenuViewController.h"
#import "MFSideMenu.h"
#import "HomeViewController.h"

@implementation SideMenuViewController

@synthesize sideMenu;

- (void) viewDidLoad {
    [super viewDidLoad];
    CGRect searchBarFrame = CGRectMake(0, 0, self.tableView.frame.size.width, 45.0);
    UISearchBar *searchBar = [[UISearchBar alloc] initWithFrame:searchBarFrame];
    searchBar.delegate = self;
    self.tableView.tableHeaderView = searchBar;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return [NSString stringWithFormat:@"Section %d", section];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    cell.textLabel.text = [NSString stringWithFormat:@"Item %d", indexPath.row];
    
    return cell;
}


#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    HomeViewController *demoController = [[HomeViewController alloc] initWithNibName:@"HomeViewController" bundle:nil];
    demoController.title = [NSString stringWithFormat:@"Demo Controller #%d-%d", indexPath.section, indexPath.row];
    
    NSArray *controllers = [NSArray arrayWithObject:demoController];
    self.sideMenu.navigationController.viewControllers = controllers;
    [self.sideMenu setMenuState:MFSideMenuStateHidden];
}

#pragma mark - UISearchBarDelegate

- (BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar {
    return YES;
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
}

@end*/
