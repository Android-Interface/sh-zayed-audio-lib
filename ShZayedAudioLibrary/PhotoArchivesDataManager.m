//
//  BookDatabaseManager.m
//  Yomiyat
//
//  Created by Aisha on 5/4/13.
//  Copyright (c) 2013 Aisha. All rights reserved.
//

#import "PhotoArchivesDataManager.h"
#import "AudioArchive.h"
@implementation PhotoArchivesDataManager
@synthesize photoArchivesArray,documentArchivesArray;

- (id)init
{
    self = [super init];
    if (self) {
        NSData *dbFile = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:@"http://apps.na.ae/ncdr/ncdr-app/NCDRApp/News/AudioLibrary/AudioDatabase.xml"]];
     //        NSData *dbFile = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:@"http://apps.na.ae/ncdr/NCDRApp/PhotoArchives/ArchiveDataBase.sqlite"]];
        //http://7arf.net/AudioLibrary/AudioDatabase.xml
        //http://apps.na.ae/ncdr/ncdr-app/NCDRApp/News/AudioLibrary/AudioDatabase.xml
        NSString *resourceDocPath = [[NSString alloc] initWithString:[[[[NSBundle mainBundle]  resourcePath] stringByDeletingLastPathComponent] stringByAppendingPathComponent:@"Documents"]];
        
        NSString *filePath = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject] stringByAppendingPathComponent:@"AudioDatabase.sqlite"];
        
        
        [dbFile writeToFile:filePath atomically:YES];
        
        dbManager=[[DBManager alloc] initWithDatabaseNameForPath:@"AudioDatabase.sqlite"];
        //dbManager=[[DBManager alloc] initWithDatabaseName:@"AudioDatabase.sqlite"];
        selectQuery=0;
    }
    return self;
}

//Arabic

-(NSMutableArray *)  archivesArabicFromDate:(NSString *) fromDate toDate:(NSString *) toDate inCategoryA:(NSString *)categoryA withKeywordA:(NSString *)keywordA atPlaceA:(NSString *)placeA{
    self.photoArchivesArray=nil;
    selectQuery=0;

    photoArchivesArray=[[NSMutableArray alloc] init];
  
    if (![fromDate isEqualToString:@"الكل"]&&![fromDate isEqualToString:@"من عام"]) {
        if (![toDate isEqualToString:@"الكل"]&&![toDate isEqualToString:@"إلى عام"]) {
            fromDate =[NSString stringWithFormat:@" AND (YEARFROM='%@'OR YEARTO='%@' OR YEARFROM='%@'OR YEARTO='%@') ",fromDate,fromDate,toDate,toDate];
            toDate=@"";
        }
        else{
            fromDate =[NSString stringWithFormat:@" AND (YEARFROM='%@'OR YEARTO='%@') ",fromDate,fromDate];
            toDate=@"";
            
        }
    }
  
    else if (![toDate isEqualToString:@"الكل"]&&![toDate isEqualToString:@"إلى عام"]) {
        if (![fromDate isEqualToString:@"الكل"]&&![fromDate isEqualToString:@"من عام"]) {
            toDate =[NSString stringWithFormat:@" AND (YEARFROM='%@'OR YEARTO='%@' OR YEARFROM='%@'OR YEARTO='%@') ",fromDate,fromDate,toDate,toDate];
            fromDate=@"";
            
        }
        else{
            toDate =[NSString stringWithFormat:@" AND (YEARFROM='%@'OR YEARTO='%@') ",toDate,toDate];
            fromDate=@"";
            
        }
    }
    else{
        toDate =@"";
        fromDate = @"";
    }

    if (![categoryA isEqualToString:@"الكل"]&&![categoryA isEqualToString:@"الموضوع"]) {
        categoryA =[NSString stringWithFormat:@" AND AUDIO_CATEGORY='%@' ",categoryA];
    }
    else{
        categoryA =@"";
        
    }
//    if (![placeA isEqualToString:@"الكل"]&&![placeA isEqualToString:@"الموقع"]) {
//        placeA =[NSString stringWithFormat:@" AND PLACEENGLISH='%@' ",placeA];
//    }
//    else{
//        placeA =@"";
//        
//    }
    
    NSString *statement=[NSString stringWithFormat:@"SELECT * from audio_archive WHERE (AUDIO_TITLE LIKE '%%%@%%' OR AUDIO_DESCRIPTION LIKE '%%%@%%' OR KEYWORD_A LIKE '%%%@%%') %@%@%@;",keywordA,keywordA,keywordA,toDate,fromDate,categoryA];

    [dbManager selectOperation:self sqlStatementString:statement];
    return photoArchivesArray;
}


//Arabic

-(NSMutableArray *)  archivesArabicWithCategoryA:(NSString *)categoryA withKeywordA:(NSString *)keywordA toDate:(NSString *) toDate andFromDate:(NSString*)fromDate{
    self.photoArchivesArray=nil;
    selectQuery=0;
    
    photoArchivesArray=[[NSMutableArray alloc] init];
    
    NSString *startDate = fromDate;
    NSString *endDate = toDate;
    
    if (![toDate isEqualToString:@"الكل"]&&![toDate isEqualToString:@"إلى عام"] && toDate!=NULL && toDate!=nil) {

            toDate =[NSString stringWithFormat:@" AND (AUDIO_YEAR<='%@'OR AUDIO_YEARTO<='%@') ",toDate,toDate];

    }
    else{
        toDate =@"";

    }
    if (![fromDate isEqualToString:@"الكل"]&&![fromDate isEqualToString:@"من عام"]&& fromDate!=NULL && fromDate!=nil) {
        
        fromDate =[NSString stringWithFormat:@" AND (AUDIO_YEAR >='%@'OR AUDIO_YEARTO >='%@') ",fromDate,fromDate];
        
    }
    else{
        fromDate =@"";
        
    }
    if(![fromDate isEqualToString:@""]&&![toDate isEqualToString:@""]){
       fromDate = [NSString stringWithFormat:@"AND (AUDIO_YEAR between %@ and %@ or AUDIO_YEARTO between %@ and %@)",startDate, endDate,startDate,endDate];
        toDate =@"";


    }
    if (![categoryA isEqualToString:@"الكل"]&&![categoryA isEqualToString:@"الموضوع"]) {
        categoryA =[NSString stringWithFormat:@" AND (AUDIO_SUBJECT='%@' OR AUDIO_SUBJECT2='%@'  OR AUDIO_SUBJECT3='%@')",categoryA,categoryA,categoryA];
    }
    else{
        categoryA =@"";
        
    }
    //    if (![placeA isEqualToString:@"الكل"]&&![placeA isEqualToString:@"الموقع"]) {
    //        placeA =[NSString stringWithFormat:@" AND PLACEENGLISH='%@' ",placeA];
    //    }
    //    else{
    //        placeA =@"";
    //
    //    }
    
    NSString *statement=[NSString stringWithFormat:@"SELECT * from audio_archive WHERE (AUDIO_TITLE LIKE '%%%@%%' OR AUDIO_DESCRIPTION LIKE '%%%@%%' OR KEYWORD_A LIKE '%%%@%%') %@%@%@;",keywordA,keywordA,keywordA,categoryA,fromDate,toDate];
    
    [dbManager selectOperation:self sqlStatementString:statement];
    return photoArchivesArray;
}



//dropdown arrays



-(NSMutableArray *) listofPhotoCategoriesArabic{
    selectQuery=20;

    self.photoArchivesArray=nil;
    photoArchivesArray=[[NSMutableArray alloc] init];
    NSString *statement=[NSString stringWithFormat:@"SELECT DISTINCT CATEGORY_TITLE from AUDIO_CATEGORY ORDER BY CATEGORY_TITLE ASC;"];
    [dbManager selectOperation:self sqlStatementString:statement];
    return photoArchivesArray;
}



-(NSMutableArray *) listofPhotoDates{
    selectQuery=20;
    
    self.photoArchivesArray=nil;
    photoArchivesArray=[[NSMutableArray alloc] init];
    NSString *statement=[NSString stringWithFormat:@"select DISTINCT audio_year as BothColumns from audio_archive union select audio_yearto from audio_archive;"];
    [dbManager selectOperation:self sqlStatementString:statement];
    return photoArchivesArray;
    
}


-(NSMutableArray *) maximumAndMinimumPhotoArchiveDates{
    selectQuery=30;
    
    self.photoArchivesArray=nil;
    photoArchivesArray=[[NSMutableArray alloc] init];
    NSString *statement=[NSString stringWithFormat:@"SELECT MAX(YEAR_TO),MIN(YEAR_FROM) FROM AUDIO_ARCHIVE "];
    [dbManager selectOperation:self sqlStatementString:statement];
    return photoArchivesArray;

}



#pragma mark SelectOperationDelegate functions

-(void) selectSuccessfullWithRow:(sqlite3_stmt*) resultRow
{
    
    if (selectQuery==10) {//documents in each document archive
        NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
        
        [dict setObject:[NSNumber numberWithInt:sqlite3_column_int(resultRow,0) ] forKey:@"documentID"];
        if (sqlite3_column_text(resultRow,1)!=NULL ) {
            [dict setObject:[NSString stringWithUTF8String:(char*)sqlite3_column_text(resultRow,1)] forKey:@"fileLink"];
            
        }
        if (sqlite3_column_text(resultRow,2)!=NULL ) {
            [dict setObject:[NSString stringWithUTF8String:(char*)sqlite3_column_text(resultRow,2)] forKey:@"fileName"];
            
        }
        
        
        [photoArchivesArray addObject:dict];
    }
    else if (selectQuery==20) {//list of places,categories
        if (sqlite3_column_text(resultRow,0)!=NULL ) {
            [photoArchivesArray addObject:[NSString stringWithUTF8String:(char*)sqlite3_column_text(resultRow,0)]];
            
        }
    }
    else if (selectQuery==30) {//min & max dates
        NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
        if (sqlite3_column_text(resultRow,0)!=NULL ) {
            [dict setObject:[NSString stringWithUTF8String:(char*)sqlite3_column_text(resultRow,0)] forKey:@"YearTo"];
            
        }
        if (sqlite3_column_text(resultRow,1)!=NULL ) {
            [dict setObject:[NSString stringWithUTF8String:(char*)sqlite3_column_text(resultRow,1)] forKey:@"YearFrom"];
            
        }
        if (sqlite3_column_text(resultRow,0)==NULL && sqlite3_column_text(resultRow,1)==NULL) {

        }
        else{
            [photoArchivesArray addObject:dict];

        }
    }
    else{//general fetching
        AudioArchive *archive = [[AudioArchive alloc] init];
            archive.audioID=[[NSNumber numberWithInt:sqlite3_column_int(resultRow,0) ]intValue];
        
        if (sqlite3_column_text(resultRow,1)!=NULL ) {
            archive.audioSubject=[NSString stringWithUTF8String:(char*)sqlite3_column_text(resultRow,1)] ;
            
        }
        if (sqlite3_column_text(resultRow,2)!=NULL ) {
            archive.audioSubject2=[NSString stringWithUTF8String:(char*)sqlite3_column_text(resultRow,2)] ;
            
        }
        if (sqlite3_column_text(resultRow,3)!=NULL ) {
            archive.audioSubject3=[NSString stringWithUTF8String:(char*)sqlite3_column_text(resultRow,3)];
            
        }
        if (sqlite3_column_text(resultRow,4)!=NULL ) {
            archive.audioFile=[NSString stringWithUTF8String:(char*)sqlite3_column_text(resultRow,4)] ;
            
        }
        if (sqlite3_column_text(resultRow,5)!=NULL ) {
            archive.iconFile=[NSString stringWithUTF8String:(char*)sqlite3_column_text(resultRow,5)] ;
            
        }
        if (sqlite3_column_text(resultRow,6)!=NULL ) {
            archive.keywordA=[NSString stringWithUTF8String:(char*)sqlite3_column_text(resultRow,6)] ;
            
        }
        if (sqlite3_column_text(resultRow,7)!=NULL ) {
            archive.audioDescription=[NSString stringWithUTF8String:(char*)sqlite3_column_text(resultRow,7)] ;
            
        }
        if (sqlite3_column_text(resultRow,8)!=NULL ) {
            archive.audioTitle=[NSString stringWithUTF8String:(char*)sqlite3_column_text(resultRow,8)] ;
            
        }
        if (sqlite3_column_text(resultRow,9)!=NULL ) {
            archive.audioDay=[NSString stringWithUTF8String:(char*)sqlite3_column_text(resultRow,9)] ;
            
        }
        if (sqlite3_column_text(resultRow,10)!=NULL ) {
            archive.audioMonth=[NSString stringWithUTF8String:(char*)sqlite3_column_text(resultRow,10)] ;
            
        }
        if (sqlite3_column_text(resultRow,11)!=NULL ) {
            archive.audioYear=[NSString stringWithUTF8String:(char*)sqlite3_column_text(resultRow,11)] ;
            
        }
        if (sqlite3_column_text(resultRow,12)!=NULL ) {
            archive.audioYearTo=[NSString stringWithUTF8String:(char*)sqlite3_column_text(resultRow,12)] ;
            
        }
        if (sqlite3_column_text(resultRow,13)!=NULL ) {
            archive.audioNotes=[NSString stringWithUTF8String:(char*)sqlite3_column_text(resultRow,13)] ;
            
        }
        [photoArchivesArray addObject:archive];

      /*  NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
        [dict setObject:[NSNumber numberWithInt:sqlite3_column_int(resultRow,0) ] forKey:@"photoID"];

        
        if (sqlite3_column_text(resultRow,1)!=NULL ) {
            [dict setObject:[NSString stringWithUTF8String:(char*)sqlite3_column_text(resultRow,1)] forKey:@"captionArabic"];
            
        }
        if (sqlite3_column_text(resultRow,2)!=NULL ) {
            [dict setObject:[NSString stringWithUTF8String:(char*)sqlite3_column_text(resultRow,2)] forKey:@"captionEnglish"];
            
        }
        if (sqlite3_column_text(resultRow,3)!=NULL ) {
            [dict setObject:[NSString stringWithUTF8String:(char*)sqlite3_column_text(resultRow,3)] forKey:@"categoryArabic"];
            
        }
        if (sqlite3_column_text(resultRow,4)!=NULL ) {
            [dict setObject:[NSString stringWithUTF8String:(char*)sqlite3_column_text(resultRow,4)] forKey:@"categoryEnglish"];
            
        }
        if (sqlite3_column_text(resultRow,5)!=NULL ) {
            [dict setObject:[NSString stringWithUTF8String:(char*)sqlite3_column_text(resultRow,5)] forKey:@"imageFile"];
            
        }
        if (sqlite3_column_text(resultRow,6)!=NULL ) {
            [dict setObject:[NSString stringWithUTF8String:(char*)sqlite3_column_text(resultRow,6)] forKey:@"keywordsArabic"];
            
        }
        if (sqlite3_column_text(resultRow,7)!=NULL ) {
            [dict setObject:[NSString stringWithUTF8String:(char*)sqlite3_column_text(resultRow,7)] forKey:@"keywordsEnglish"];
            
        }
        if (sqlite3_column_text(resultRow,8)!=NULL ) {
            [dict setObject:[NSString stringWithUTF8String:(char*)sqlite3_column_text(resultRow,8)] forKey:@"placeArabic"];
            
        }
        if (sqlite3_column_text(resultRow,9)!=NULL ) {
            [dict setObject:[NSString stringWithUTF8String:(char*)sqlite3_column_text(resultRow,9)] forKey:@"placeEnglish"];
            
        }
        if (sqlite3_column_text(resultRow,10)!=NULL ) {
            [dict setObject:[NSString stringWithUTF8String:(char*)sqlite3_column_text(resultRow,10)] forKey:@"yearFrom"];
            
        }
        if (sqlite3_column_text(resultRow,11)!=NULL ) {
            [dict setObject:[NSString stringWithUTF8String:(char*)sqlite3_column_text(resultRow,11)] forKey:@"yearTo"];
            
        }
        [photoArchivesArray addObject:dict];
        */
    }
  /*  if(selectQuery==1)
    {
        
        if((char*)sqlite3_column_text(resultRow,0))
        {
            NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
            NSString* topic=[NSString stringWithUTF8String:(char*)sqlite3_column_text(resultRow,0)];
            [dict setObject:topic forKey:@"topic"];
            [youmiyats addObject:dict];
        }

    }
    else  if(selectQuery==2)
    {
        if((char*)sqlite3_column_text(resultRow,0))
        {
            NSString* topic=[NSString stringWithUTF8String:(char*)sqlite3_column_text(resultRow,0)];
            description=[[NSString alloc] initWithFormat:@"%@",topic];
        }
        
    }
    else  if(selectQuery==3)
    {
        if((char*)sqlite3_column_text(resultRow,0))
        {
            NSString* topic=[NSString stringWithUTF8String:(char*)sqlite3_column_text(resultRow,0)];
            latestDate=[[NSString alloc] initWithFormat:@"%@",topic];
        }
        
    }
        else//0
        {
                if((char*)sqlite3_column_text(resultRow,0))
                {
                    NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
                    
                    int numID=sqlite3_column_int(resultRow,0);
                    
                    NSString *number=[NSString stringWithFormat:@"%i",numID];
                    
                    NSString* topic=[NSString stringWithUTF8String:(char*)sqlite3_column_text(resultRow,2)];
                    
                    NSString* subtopic=[NSString stringWithUTF8String:(char*)sqlite3_column_text(resultRow,3)];
                    
                   // NSString* description=[NSString stringWithUTF8String:(char*)sqlite3_column_text(resultRow,3)];
                    
                    NSString* source=[NSString stringWithUTF8String:(char*)sqlite3_column_text(resultRow,4)];
                    
                    NSString* shortDesc=[NSString stringWithUTF8String:(char*)sqlite3_column_text(resultRow,5)];
                    
                    NSString* date=[NSString stringWithUTF8String:(char*)sqlite3_column_text(resultRow,6)];
                    [dict setObject:number forKey:@"id"];
                    [dict setObject:topic forKey:@"topic"];
                    [dict setObject:subtopic forKey:@"subtopic"];
                  //[dict setObject:description forKey:@"description"];
                    [dict setObject:source forKey:@"source"];
                    [dict setObject:shortDesc forKey:@"shortDesc"];
                    [dict setObject:date forKey:@"date"];
                    [youmiyats addObject:dict];
                }
        }*/
}

-(NSString*) getLatestDateFromTable
{
    selectQuery=3;
    latestDate=nil;
    NSString *statement=[NSString stringWithFormat:@"SELECT MAX(Date) FROM Youmyat"];//FK_User_Id=%d AND (DeleteMark IS NULL OR DeleteMark=0);",[self userID]];
    [dbManager selectOperation:self sqlStatementString:statement];
    return latestDate;
}

-(void) selectFailed
{
	NSLog(@"Select Failed");
}

@end
