//
//  main.m
//  ShZayedAudioLibrary
//
//  Created by Anam on 2/1/14.
//  Copyright (c) 2014 Interface. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
