//
//  DBManager.h
//  AnzuReader
//
//  Created by OmarHussain on 5/16/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "sqlite3.h"


/*! \brief Select Operation Protocol
 * This protocol implements the response from the SQliteDB. */
@protocol SelectOperationDelegate
@required
/*! For Successfull retrieval from the DB
 * \param resultRow sqlite3_stmt object returning from sql query
 * \return void
 */
-(void) selectSuccessfullWithRow:(sqlite3_stmt*) resultRow;

/*! For failed retrieval from the DB
 * \return void
 */
-(void) selectFailed;
@end

//@protocol InsertOperationDelegate
//@required
//-(void) insertSuccessfull;
//-(void) insertFailed;
//@end

//@protocol DeleteOperationDelegate
//@required
//-(void) deleteSuccessfull;
//-(void) deleteFailed;
//@end

//@protocol UpdateOperationDelegate
//@required
//-(void) updateSuccessfull;
//-(void) updateFailed;
//@end

/*! \brief Database Manager
 * This class acts as base class for database related operations. */

@interface DBManager : NSObject 
{
    /*! Database Path string */
	NSString* databasePath;
    
    /*! Sqlite3  object for database interaction */
	sqlite3* database;
}
@property (assign) sqlite3* database;


/*! For initializing database with name
 * \param databaseName Database Name
 * \return id
 */
-(id) initWithDatabaseName:(NSString*) databaseName;

/*! For initializing database with name for path
 * \param databaseName Database Name
 * \return id
 */
-(id) initWithDatabaseNameForPath:(NSString*) databaseName;


/*! For displaying message
 * \param title Title
 * \param message Message
 * \return void
 */
-(void) displayMessage:(NSString*) title message:(NSString*) message;

/*! For Checking and Creating Database
 * \param databaseName Database Name
 * \return BOOl indicating successfull or unsuccessfull operation
 */
-(BOOL) checkAndCreateDatabase:(NSString*) databaseName;


/*! For checking database
 * \param databaseName Database Name
 * \return BOOL indicating successfull or unsuccessfull operation
 */
-(BOOL) checkDatabase:(NSString*) databaseName;

/*! To open Database
 * \return BOOl indicating successfull or unsuccessfull operation
 */
-(BOOL) openDatabase;

/*! To close Database
  * \return BOOL indicating successfull or unsuccessfull operation
 */
-(BOOL) closeDatabase;

/*! Called in result of successfull DB retrieval
 * \param delegate id for selectOperationDelegate
 * \param sqlStatement SQL statement
 * \return BOOL indicating successfull or unsuccessfull operation
 */
-(BOOL) selectOperation:(id<SelectOperationDelegate>) delegate sqlStatementString:(NSString*) sqlStatement;

/*! For Insertion in DB
 * \param sqlStatement SQL Statement
 * \return BOOL indicating successfull or unsuccessfull operation
 */
-(BOOL) insertOperation:(NSString*) sqlStatement;

/*! For Deleting from DB
 * \param sqlStatement SQL Statement
 * \return BOOL indicating successfull or unsuccessfull operation
 */
-(BOOL) deleteOperation:(NSString*) sqlStatement;

/*! For updating info in DB
 * \param sqlStatement SQL Statement
 * \return BOOL indicating successfull or unsuccessfull operation
 */
-(BOOL) updateOperation:(NSString*) sqlStatement;

/*! For performing operation in DB
 * \param sqlStatement SQL Statement
 * \return BOOL indicating successfull or unsuccessfull operation
 */
-(BOOL) performOperation:(NSString*) sqlStatement;

/*! For checking records in DB
 * \param sqlStatement SQL Statement
 * \return BOOL indicating successfull or unsuccessfull operation
 */
-(BOOL) CheckRow:(NSString*) sqlStatement;
//-(BOOL) insertOperation:(id<InsertOperationDelegate>) delegate sqlStatementString:(NSString*) sqlStatement;
//-(BOOL) deleteOperation:(id<DeleteOperationDelegate>) delegate sqlStatementString:(NSString*) sqlStatement;
//-(BOOL) updateOperation:(id<UpdateOperationDelegate>) delegate sqlStatementString:(NSString*) sqlStatement;

/*! To get DB verstion
 * \return BOOL indicating successfull or unsuccessfull operation
 */
-(BOOL) getDBVersionState;

/*! To Set DB version in DB
 * \param state State of DB in BOOL
 * \return void
 */
-(void) setDBVersionState:(BOOL) state;

/*! For getting current Date Time
 * \return String date time info
 */
-(NSString*) getCurrentDateTime;

@end
