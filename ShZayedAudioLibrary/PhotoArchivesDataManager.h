//
//  BookDatabaseManager.h
//  Yomiyat
//
//  Created by Aisha on 5/4/13.
//  Copyright (c) 2013 Aisha. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DBManager.h"

@interface PhotoArchivesDataManager : NSObject<SelectOperationDelegate>
{
    DBManager *dbManager;
    NSMutableArray *photoArchivesArray;
    NSMutableArray *documentArchivesArray;

    int selectQuery;
    NSString *description;
    NSString *latestDate;
    
}
@property(strong,nonatomic) NSMutableArray *photoArchivesArray;
@property(strong,nonatomic) NSMutableArray *documentArchivesArray;

// English
-(NSMutableArray *) listOfArchives;
-(NSMutableArray *) archiveEnglishForId:(int) index;
-(NSMutableArray *) archivesEnglishWithKeyword:(NSString *) keywordE;
-(NSMutableArray *)  archivesEnglishFromDate:(NSString *) fromDate toDate:(NSString *) toDate inCategoryE:(NSString *)categoryE withKeywordE:(NSString *)keywordE atPlaceE:(NSString *)placeE;

//Arabic
-(NSMutableArray *) archiveArabicForId:(int) index;
-(NSMutableArray *) archivesArabicWithKeyword:(NSString *) keywordA;
-(NSMutableArray *)  archivesArabicFromDate:(NSString *) fromDate toDate:(NSString *) toDate inCategoryA:(NSString *)categoryA withKeywordA:(NSString *)keywordA atPlaceA:(NSString *)placeA;
-(NSMutableArray *)  archivesArabicWithCategoryA:(NSString *)categoryA withKeywordA:(NSString *)keywordA toDate:(NSString *) toDate;


// English
-(NSMutableArray *) listOfDocArchives;
-(NSMutableArray *) docArchiveForId:(int) index;
-(NSMutableArray *) docArchivesEnglishWithKeyword:(NSString *) keywordE;
-(NSMutableArray *)  docArchivesEnglishFromDate:(NSString *) fromDate toDate:(NSString *) toDate inCategoryE:(NSString *)categoryE withKeywordE:(NSString *)keywordE atPlaceE:(NSString *)placeE;

//Arabic
-(NSMutableArray *) docArchiveArabicForId:(int) index;
-(NSMutableArray *) docArchivesArabicWithKeyword:(NSString *) keywordA;
-(NSMutableArray *)  docArchivesArabicFromDate:(NSString *) fromDate toDate:(NSString *) toDate inCategoryA:(NSString *)categoryA withKeywordA:(NSString *)keywordA atPlaceA:(NSString *)placeA;
-(NSMutableArray *)  archivesArabicWithCategoryA:(NSString *)categoryA withKeywordA:(NSString *)keywordA toDate:(NSString *) toDate andFromDate:(NSString*)fromDate;

-(NSMutableArray*) documentsForDocumentID:(int) documentID;

//dropdown arrays
-(NSMutableArray *) listofDocumentCategoriesEnglish;
-(NSMutableArray *) listofPhotoCategoriesEnglish;

-(NSMutableArray *) listofDocumentPlacesEnglish;
-(NSMutableArray *) listofPhotoPlacesEnglish;

-(NSMutableArray *) listofDocumentCategoriesArabic;
-(NSMutableArray *) listofPhotoCategoriesArabic;

-(NSMutableArray *) listofDocumentPlacesArabic;
-(NSMutableArray *) listofPhotoPlacesArabic;

-(NSMutableArray *) maximumAndMinimumDocumentArchiveDates;
-(NSMutableArray *) maximumAndMinimumPhotoArchiveDates;
-(NSMutableArray *) listofPhotoDates;


@end
