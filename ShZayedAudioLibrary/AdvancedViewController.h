//
//  BrowseViewController.h
//  Yomiyat
//
//  Created by Aisha on 5/20/13.
//  Copyright (c) 2013 Aisha. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DropDownButton.h"
#import "Common.h"

#import "PhotoArchivesDataManager.h"
#import "AudioArchive.h"
#import "LanguageManager.h"


@interface AdvancedViewController : UIViewController<dropDownProtocol,UITextFieldDelegate,AVAudioPlayerDelegate>
{
    PhotoArchivesDataManager *archivesDB;

    IBOutlet UITableView *tableView;
    NSMutableArray *searchArray;
   
    IBOutlet UINavigationBar *navBar;
    IBOutlet UIImageView *topTitleImageView;
  
    __weak IBOutlet UIView *activityView;
    
    IBOutlet UITextField *searchTextField;
//    IBOutlet DropDownButton *placeDropDownButton;
    IBOutlet DropDownButton *categoryDropDownButton;
    IBOutlet DropDownButton *fromYearDropDownButton;
    IBOutlet DropDownButton *toYearDropDownButton;
    
    NSMutableArray *placesDropdownArray;
    NSMutableArray *categoriesDropDownArray;
    NSMutableArray *datesDropDownArray;
    
       //
    LanguageManager *languageSharedManager;

    IBOutlet UILabel *titleLabel;
    int fontSize;
    
       int currentIndex;
    
    int selectedIndexPath;

    
    IBOutlet UIImageView *tableBackgroundView;
    
    IBOutlet UIImageView *searchBackgroundImageView;
    IBOutlet UIButton *searchButton;

    IBOutlet UIButton *helpScreen;
    
    
    IBOutlet UIView *mpVolumeViewParentView;

    IBOutlet UIButton *playPauseButton;


    IBOutlet UISlider *playerSlider;

    AVAudioPlayer *audioPlayer;
    BOOL isPause;
    BOOL playAll;

    
    
    
    
}
@property (strong, nonatomic) IBOutlet UIButton *playAllButton;

-(IBAction) goBackToList:(id)sender;
-(IBAction)goNextPage:(id)sender;
-(IBAction)goPrePage:(id)sender;

-(void)dismissHelpFromView;

@end
