//
//  LanguageManager.h
//  FSVerticalTabBarExample
//
//  Created by Aisha on 1/7/13.
//
//

#import <Foundation/Foundation.h>

@interface LanguageManager : NSObject
{
    int selectedLanguage_;

}
@property(nonatomic,assign) int selectedLanguage;
+ (id)sharedManager;
@end
