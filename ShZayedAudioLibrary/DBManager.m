//
//  DBManager.m
//  AnzuReader
//
//  Created by OmarHussain on 5/16/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "DBManager.h"

@implementation DBManager

@synthesize database;

-(id) initWithDatabaseName:(NSString*) databaseName
{
	self=[super init];
	if([self checkAndCreateDatabase:databaseName])
		[self openDatabase];
	return self;
}

-(id) initWithDatabaseNameForPath:(NSString*) databaseName
{
	self=[super init];
	if([self checkDatabase:databaseName])
		[self openDatabase];
	return self;
}

-(void) displayMessage:(NSString*) title message:(NSString*) message
{
	UIAlertView* alertView=[[UIAlertView alloc] initWithTitle:title message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
	[alertView show];
	[alertView release];	
}

-(BOOL) checkDatabase:(NSString*) databaseName 
{  
	NSArray* documentPaths=NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask,YES);
	NSString* documentsDirectory=[documentPaths objectAtIndex:0];
	databasePath=[documentsDirectory stringByAppendingPathComponent:databaseName];
	//NSLog(@"not existing for path %@",databasePath);
	if([[NSFileManager defaultManager] fileExistsAtPath:databasePath])
		return YES;
	else
	{
		[self displayMessage:@"Error" message:@"Can not open publication database."];
		return NO;
	}
}

-(BOOL) checkAndCreateDatabase:(NSString*) databaseName 
{   // if(!flag)
    {
        NSArray* documentPaths=NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask,YES);
        NSString* documentsDirectory=[documentPaths objectAtIndex:0];
        databasePath=[documentsDirectory stringByAppendingPathComponent:databaseName];
      //  DLog(@"not existing for path %@",databasePath);
        if([[NSFileManager defaultManager] fileExistsAtPath:databasePath])
            return YES;
        NSString* databasePathInBundle=[[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:databaseName];
        if([[NSFileManager defaultManager] copyItemAtPath:databasePathInBundle toPath:databasePath error:nil]==NO)
        {
            [self displayMessage:@"Error" message:@"Failed to copy database to application folder."];
            return NO;
        }
		//[self setDBVersionState:NO];
        [self addSkipBackupAttributeToItemAtURL:[NSURL fileURLWithPath:databasePath]];
        return YES;
    }
}


-(BOOL) openDatabase
{
	if(sqlite3_open([databasePath UTF8String],&database)!=SQLITE_OK)
	{
		sqlite3_close(database);
		database=NULL;
		[self displayMessage:@"Error" message:@"Failed to open database."];
		return NO;
	}
	else    
	{
		//if(![self getDBVersionState])
		//	[self setDBVersionState:YES];
		//NSLog(@"Database opened successfully.");
		return YES;
	}
	return YES;
}


-(BOOL) closeDatabase
{
	if(database==NULL)
		return YES;
	if(sqlite3_close(database)!=SQLITE_OK)
	{
		[self displayMessage:@"Error" message:@"Faied to close database."];
		return NO;
	}
	else
	{
		//NSLog(@"Database closed successfully.");
		return YES;
	}
	return YES;
}

-(BOOL) selectOperation:(id<SelectOperationDelegate>) delegate sqlStatementString:(NSString*) sqlStatement
{
	sqlite3_stmt* compiledStatement=NULL;
	BOOL isSuccess=NO;
	if(database==NULL)
		return isSuccess;
	if(sqlite3_prepare_v2(database,[sqlStatement UTF8String],-1,&compiledStatement,NULL)==SQLITE_OK)
	{
		while(sqlite3_step(compiledStatement)==SQLITE_ROW)
		{
			[delegate selectSuccessfullWithRow:compiledStatement];
		}
		isSuccess=YES;
	}
	else
	{
		[delegate selectFailed];
		isSuccess=NO;
	}
    
	if(compiledStatement)
		sqlite3_finalize(compiledStatement);
	return isSuccess;
}

//-(BOOL) insertOperation:(id<InsertOperationDelegate>) delegate sqlStatementString:(NSString*) sqlStatement
-(BOOL) insertOperation:(NSString*) sqlStatement
{
	return [self performOperation:sqlStatement];
}

//-(BOOL) deleteOperation:(id<DeleteOperationDelegate>) delegate sqlStatementString:(NSString*) sqlStatement
-(BOOL) deleteOperation:(NSString*) sqlStatement
{
	return [self performOperation:sqlStatement];
}

//-(BOOL) updateOperation:(id<UpdateOperationDelegate>) delegate sqlStatementString:(NSString*) sqlStatement
-(BOOL) updateOperation:(NSString*) sqlStatement
{
	return [self performOperation:sqlStatement];
}

-(BOOL) performOperation:(NSString*) sqlStatement
{
	sqlite3_stmt* compiledStatement=NULL;
	BOOL isSuccess=NO;
	if(database==NULL)
		return isSuccess;
	if(sqlite3_prepare_v2(database,[sqlStatement UTF8String],-1,&compiledStatement,NULL)==SQLITE_OK)
	{
		if(sqlite3_step(compiledStatement)==SQLITE_DONE)
		{
			isSuccess=YES;
		}
		else
		{
			isSuccess=NO;
		}	
	}
	if(compiledStatement)
		sqlite3_finalize(compiledStatement);
	return isSuccess;
}

-(BOOL) CheckRow:(NSString*) sqlStatement
{
	sqlite3_stmt* compiledStatement=NULL;
	BOOL isSuccess=NO;
	if(database==NULL)
		return isSuccess;
	if(sqlite3_prepare_v2(database,[sqlStatement UTF8String],-1,&compiledStatement,NULL)==SQLITE_OK)
	{
		if(sqlite3_step(compiledStatement)==SQLITE_ROW)
		{
			isSuccess=YES;
		}
		else
		{
			isSuccess=NO;
		}	
	}
	if(compiledStatement)
		sqlite3_finalize(compiledStatement);
	return isSuccess;
}

-(BOOL) getDBVersionState
{
	return [[NSUserDefaults standardUserDefaults] boolForKey:@"ShouldInsertDBVersion"];
}

-(void) setDBVersionState:(BOOL) state
{
	if(state==YES)
	{
		NSString* insertString=[NSString stringWithFormat:@"INSERT INTO SYSTEM (DBVersion,LastUpdatedDate) VALUES ('%@','%@');",@"0",[self getCurrentDateTime]];
		if(![self insertOperation:insertString])
			return;
	}
    
	[[NSUserDefaults standardUserDefaults] setBool:state forKey:@"ShouldInsertDBVersion"];
	[[NSUserDefaults standardUserDefaults] synchronize];
}

-(NSString*) getCurrentDateTime
{
	NSDate* currentDateTime=[NSDate date];
	NSDateFormatter* dateFormatter=[[[NSDateFormatter alloc] init] autorelease];
	NSTimeZone* timeZone=[NSTimeZone timeZoneWithName:@"UTC"];
    [dateFormatter setTimeZone:timeZone];
	[dateFormatter setDateFormat:@"yyyy/MM/dd HH:mm:ss"];
	NSString* formatedDateTime=[dateFormatter stringFromDate:currentDateTime];
	return formatedDateTime;
}


/*-(NSString*) getCurrentDateTime
{
	NSDate* currentDateTime=[NSDate date];
	NSDateFormatter* dateFormatter=[[[NSDateFormatter alloc] init] autorelease];
	NSTimeZone* timeZone=[NSTimeZone timeZoneWithName:@"UTC"];
    [dateFormatter setTimeZone:timeZone];
	[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
	NSString* formatedDateTime=[dateFormatter stringFromDate:currentDateTime];
	return formatedDateTime;
}*/



- (BOOL)addSkipBackupAttributeToItemAtURL:(NSURL *)URL
{
    assert([[NSFileManager defaultManager] fileExistsAtPath: [URL path]]);
    
    NSError *error = nil;
    BOOL success = [URL setResourceValue: [NSNumber numberWithBool: YES]
                                  forKey: NSURLIsExcludedFromBackupKey error: &error];
    if(!success){
        NSLog(@"Error excluding %@ from backup %@", [URL lastPathComponent], error);
    }
    return success;
}


-(void) dealloc
{
	[self closeDatabase];
	[super dealloc];
}

@end
