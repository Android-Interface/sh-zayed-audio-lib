//
//  MoreAppsViewController.h
//  ShZayedAudioLibrary
//
//  Created by Anam on 2/12/14.
//  Copyright (c) 2014 Interface. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LanguageManager.h"
#import "MoreApp.h"
#import "MoreAppsDataManager.h"
#import <StoreKit/StoreKit.h>

@interface MoreAppsViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,SKStoreProductViewControllerDelegate>
{
    NSMutableArray *moreAppsArray;
    MoreAppsDataManager *moreAppsDataManager;
    LanguageManager *languageSharedManager;

}
@property (strong, nonatomic) IBOutlet UILabel *mainTitleLabel;
@property (strong, nonatomic) IBOutlet UITableView *tableView;

@end
