//
//  Common.h
//  FSVerticalTabBarExample
//
//  Created by Aisha on 12/30/12.
//
//

#ifndef FSVerticalTabBarExample_Common_h
#define FSVerticalTabBarExample_Common_h


//NSString* languageSelectedStringForKeyArabic(NSString* key)
//{
//    // [self test];
//    int  selectedLanguage=2;
//	NSString *path;
//	if(selectedLanguage==1)
//		path = [[NSBundle mainBundle] pathForResource:@"English" ofType:@"lproj"];
//        else if(selectedLanguage==2)
//            path = [[NSBundle mainBundle] pathForResource:@"zh" ofType:@"lproj"];
//            
//            NSBundle* languageBundle = [NSBundle bundleWithPath:path];
//            NSString* str=[languageBundle localizedStringForKey:key value:@"" table:nil];
//            return str;
//}
//
//NSString* languageSelectedStringForKey(NSString* key)
//{
//    // [self test];
//    int  selectedLanguage=1;
//	NSString *path;
//	if(selectedLanguage==1)
//		path = [[NSBundle mainBundle] pathForResource:@"English" ofType:@"lproj"];
//        else if(selectedLanguage==2)
//            path = [[NSBundle mainBundle] pathForResource:@"zh" ofType:@"lproj"];
//            
//            NSBundle* languageBundle = [NSBundle bundleWithPath:path];
//            NSString* str=[languageBundle localizedStringForKey:key value:@"" table:nil];
//            return str;
//}
//




#define NUMBER_OF_ROWS 6
#define THUMBNAIL_WIDTH 75
#define THUMBNAIL_HEIGHT 75
#define GALLERY_INDEX 7
#define LOCATION_INDEX 6
#define FLEXIBLE_WID_P 280
#define FLEXIBLE_WID_L 500
//WEB CONTENT
#define WEB_VIEW_HEIGHT_P 632
#define WEB_VIEW_HEIGHT_L 376
#define IMAGE_VIEW_HEIGHT_WITH_OFFSET 135
#define TAB_BAR_HEIGHT 49

#define NAME_LABEL_OFFSET 0
#define WEB_VIEW_TOP 185+NAME_LABEL_OFFSET
#define NUMBER_IF_GALLERY_IMAGES 27
#define BOTTOM_LINE_TOP 807-49
#define CONTENT_START 320
#define VIEW_HEIGHT 1024
#define VIEW_WIDTH 768
#define RADIANS(degrees) ((degrees * M_PI) / 180.0)
#define THUMMBNAIL_TAG 177
#define ABOUTDROPDOWNINDICATOR_TAG 180


#define IS_IPHONE (!IS_IPAD)
#define IS_IPAD (UI_USER_INTERFACE_IDIOM() != UIUserInterfaceIdiomPhone)

//#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
int globaltextFontSize;
int languageChange;


#define SERVER_PATH_GALLERY @"http://apps.na.ae/ncdr/ncdr-app/NCDRApp/Gallery/"//@"http://interface-uae.com/NCDRApp/Gallery/"
//#define SERVER_PATH_NEWS @"http://ncdr.ae/NCDRApp/Gallery/"
//@"http://ncdr.ae/NCDRApp/Gallery/"

#define NCDR_ARABIC_NEWS @"http://apps.na.ae/ncdr/NCDR-App/NCDRApp/News/NCDR_NEWS_ARABIC.xml"
#define NCDR_ENGLISH_NEWS @"http://apps.na.ae/ncdr/NCDR-App/NCDRApp/News/NCDR_NEWS_ENGLISH.XML"

//

//http://apps.na.ae/ncdr/ncdr-app/NCDRApp/Books/AboutNCDR/NCDR.plist

#define PUBLISHER_LATEST_PUBLICATIONS_URL @"http://apps.na.ae/ncdr/ncdr-app/NCDRApp/Books/LatestPublications/LatestPublications.plist"
//#define PUBLISHER_LATEST_PUBLICATIONS_URL @"http://apps.na.ae/ncdr/ncdr-app/NCDRApp/Books/Liwa/Liwa.plist"
#define PUBLISHER_PUBLICATIONS_URL @"http://apps.na.ae/ncdr/ncdr-app/NCDRApp/Books/Liwa/Liwa.plist"
#define PUBLISHER_LIWA_URL @"http://apps.na.ae/ncdr/ncdr-app/NCDRApp/Books/Publications/Publications.plist"
#define PUBLISHER_THE_MEMORY_URL @"http://apps.na.ae/ncdr/ncdr-app/NCDRApp/Books/AboutNCDR/NCDR.plist"
#define PUBLISHER_ABOUT_NCDR_URL @"http://apps.na.ae/ncdr/ncdr-app/NCDRApp/Books/TheMemory/TheMemory.plist"
#define PUBLISHER_KNOWLEDGE_SERVICES_URL @"http://apps.na.ae/ncdr/ncdr-app/NCDRApp/KnwoledgeServices/KnowledgeServices.plist"


#define PUBLISHER_LATEST_PUBLICATIONS_URL_ARABIC @"http://apps.na.ae/ncdr/ncdr-app/NCDRApp/Books/LatestPublications/LatestPublicationsArabic.plist"
//#define PUBLISHER_LATEST_PUBLICATIONS_URL @"http://apps.na.ae/ncdr/ncdr-app/NCDRApp/Books/Liwa/Liwa.plist"
#define PUBLISHER_PUBLICATIONS_URL_ARABIC @"http://apps.na.ae/ncdr/ncdr-app/NCDRApp/Books/Liwa/LiwaArabic.plist"
#define PUBLISHER_LIWA_URL_ARABIC @"http://apps.na.ae/ncdr/ncdr-app/NCDRApp/Books/Publications/PublicationsArabic.plist"
#define PUBLISHER_THE_MEMORY_URL_ARABIC @"http://apps.na.ae/ncdr/ncdr-app/NCDRApp/Books/TheMemory/TheMemoryArabic.plist"
#define PUBLISHER_ABOUT_NCDR_URL_ARABIC @"http://apps.na.ae/ncdr/ncdr-app/NCDRApp/Books/AboutNCDR/NCDRArabic.plist"


#define PUBLISHER_LATEST_PUBLICATIONS_URL_ENGLISH @"http://apps.na.ae/ncdr/ncdr-app/NCDRApp/Books/LatestPublications/LatestPublicationsArabic.plist"
//#define PUBLISHER_LATEST_PUBLICATIONS_URL @"http://apps.na.ae/ncdr/ncdr-app/NCDRApp/Books/Liwa/Liwa.plist"
#define PUBLISHER_PUBLICATIONS_URL_ENGLISH @"http://apps.na.ae/ncdr/ncdr-app/NCDRApp/Books/Liwa/LiwaEnglish.plist"
#define PUBLISHER_LIWA_URL_ENGLISH @"http://apps.na.ae/ncdr/ncdr-app/NCDRApp/Books/Publications/PublicationsEnglish.plist"
#define PUBLISHER_THE_MEMORY_URL_ENGLISH @"http://apps.na.ae/ncdr/ncdr-app/NCDRApp/Books/TheMemory/TheMemoryEnglish.plist"
#define PUBLISHER_ABOUT_NCDR_URL_ENGLISH @"http://apps.na.ae/ncdr/ncdr-app/NCDRApp/Books/AboutNCDR/NCDREnglsih.plist"

//
/*
#define PUBLISHER_LATEST_PUBLICATIONS_URL @"http://apps.na.ae/ncdr/ncdr-app/NCDRApp/Books/LatestPublications/LatestPublications.plist"
#define PUBLISHER_PUBLICATIONS_URL @"http://interface-uae.com/webApplication14/Books/Liwa/Liwa.plist"
#define PUBLISHER_LIWA_URL @"http://interface-uae.com/webApplication14/Books/Publications/Publications.plist"
#define PUBLISHER_THE_MEMORY_URL @"http://interface-uae.com/webApplication14/Books/TheMemory/TheMemory.plist"
#define PUBLISHER_ABOUT_NCDR_URL @"http://interface-uae.com/webApplication14/Books/AboutNCDR/NCDR.plist"
#define PUBLISHER_KNOWLEDGE_SERVICES_URL @"http://apps.na.ae/ncdr/ncdr-app/NCDRApp/KnwoledgeServices/KnowledgeServices.plist"
//*/

//apps.na.ae/ncdr/NCDR-App/NCDRApp/News/NCDR_NEWS_ARABIC.XML
//apps.na.ae/ncdr/NCDR-App/NCDRApp/News/NCDR_NEWS_ENGLISH.XML

#define kINSTAGRAMAUTHURL @"https://api.instagram.com/oauth/authorize/"
#define kAINSTAGRAMPIURl @"https://api.instagram.com/v1/users/"
#define KINSTAGRAMCLIENTID @"6ba0c0220e744b4b8cb71e5a04419b30"
#define KINSTAGRAMCLIENTSERCRET @"01882e7d2330430f80867526104ea47c"
#define kINSTAGRAMREDIRECTURI @"http://instagram.com/ncdr_uae"

#define DELETE_BTN_TAG 500
#define IMAGE_VIEW_TAG 1000
#define PROGRESS_VIEW_TAG 1500
#define LABEL_VIEW_TAG 2000
#define PURCHASE_BUTTON_TAG 2500
#define INFO_BUTTON_TAG 3000
#define TITLE_LABEL_TAG 3500
#define LOCK_IMAGE_VIEW_TAG 4000
#define DISCOUNT_IMAGE_VIEW_TAG 4500

#define BACK @"العودة"
#define SEARCH @"بحث"
#define GALLERY @"معرض الصور"
#define TODAY @"اليوم"
#define DATE @"تاريخ"
#define EVENT @"حدث"
#define ALL  @"الكل"

#endif
