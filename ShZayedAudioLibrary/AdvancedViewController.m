//
//  BrowseViewController.m
//  Yomiyat
//
//  Created by Aisha on 5/20/13.
//  Copyright (c) 2013 Aisha. All rights reserved.
//

#import "AdvancedViewController.h"
//#import "YoumiyatsListsViewController.h"
//#import "BrowseByDateViewController.h"
#import "Common.h"
#import "CustomCell.h"
//#import "TextViewViewController.h"
#import "MyUtilities.h"
#import "CustomCellHome.h"
#import "MoreAppsViewController.h"

#import "ArchivesSearchResultsViewController.h"

#define  REDCOLOR [[UIColor alloc] initWithRed:0.43 green:0.14 blue:0.17 alpha:1]
@interface AdvancedViewController ()

@property(nonatomic,strong) UIImage *placeholder;


@end

@implementation AdvancedViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {

      
    }
    return self;
}
- (IBAction)backButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation duration:(NSTimeInterval)duration
{
    
}



- (IBAction)dismissKeyPad:(id)sender {
    [searchTextField resignFirstResponder];
}

- (void)processCompleted{
	
}

- (void)processHasErrors
{
    UIAlertView *alertView=[[UIAlertView alloc] initWithTitle:@"Connectivity failed" message:@"No Gallery fetched from server" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
    [alertView show];
}

#pragma View Appear
-(void) viewWillDisappear:(BOOL)animated
{
    [audioPlayer stop];
    audioPlayer=nil;
}
-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.backItem.title = @"العودة";

    selectedIndexPath = -1;
    
  

    if ([searchTextField.text isEqualToString:@""]) {
        toYearDropDownButton.enabled=YES;
        fromYearDropDownButton.enabled=YES;
        categoryDropDownButton.enabled=YES;
        if (searchArray.count>0) {
            self.playAllButton.enabled = YES;
            
        }
//        placeDropDownButton.enabled=NO;
    }
    else{
        toYearDropDownButton.enabled=YES;
        fromYearDropDownButton.enabled=YES;
        categoryDropDownButton.enabled=YES;
        if (searchArray.count>0) {
            self.playAllButton.enabled = YES;
            
        }
    }

    
    
    
             //   [placeDropDownButton setBackgroundImage:[UIImage imageNamed:@"filter A.png"] forState:UIControlStateNormal];
                [categoryDropDownButton setBackgroundImage:[UIImage imageNamed:@"iPhone5 Larg filter.png"] forState:UIControlStateNormal];
                [fromYearDropDownButton setBackgroundImage:[UIImage imageNamed:@"iPhone5 Year filter.png"] forState:UIControlStateNormal];
                [toYearDropDownButton setBackgroundImage:[UIImage imageNamed:@"iPhone5 Year filter.png"] forState:UIControlStateNormal];
                
    
    
    
    
    
   
       [fromYearDropDownButton setTitle:[self languageSelectedStringForKeyArabic:@"Start Date"] forState:UIControlStateNormal];
        [toYearDropDownButton setTitle:[self languageSelectedStringForKeyArabic:@"End Date"] forState:UIControlStateNormal];
     //   [placeDropDownButton setTitle:[self languageSelectedStringForKeyArabic:@"Location"] forState:UIControlStateNormal];
        [categoryDropDownButton setTitle:[self languageSelectedStringForKeyArabic:@"Category"] forState:UIControlStateNormal];
    
    UIImage *image=[UIImage imageNamed:@"iPhone5 Larg filter_sub 1.png"];
    UIImage *image2=[UIImage imageNamed:@"iPhone5 Samll filter_sub 1.png"];
    [categoryDropDownButton initButtonWithData:categoriesDropDownArray parentTarget:self toView:self.view cellBackground:image];
   // [placeDropDownButton initButtonWithData:placesDropdownArray parentTarget:self toView:self.view cellBackground:image];
    [toYearDropDownButton initButtonWithData:datesDropDownArray parentTarget:self toView:self.view cellBackground:image];
    [fromYearDropDownButton initButtonWithData:datesDropDownArray parentTarget:self toView:self.view cellBackground:image];

    self.navigationController.navigationBarHidden=NO;
    [self makeFilterComponentsForSearch];
    [self openSearchResult:nil];
   // [self performSelector:@selector(startLoadingData) withObject:nil afterDelay:0.5];

}
-(NSString*) languageSelectedStringForKeyArabic:(NSString*) key
{
    // [self test];
    int  selectedLanguage=2;
	NSString *path;
	if(selectedLanguage==1)
		path = [[NSBundle mainBundle] pathForResource:@"English" ofType:@"lproj"];
	else if(selectedLanguage==2)
		path = [[NSBundle mainBundle] pathForResource:@"zh" ofType:@"lproj"];
    
	NSBundle* languageBundle = [NSBundle bundleWithPath:path];
	NSString* str=[languageBundle localizedStringForKey:key value:@"" table:nil];
	return str;
}

-(NSString*) languageSelectedStringForKey:(NSString*) key
{
    // [self test];
    int  selectedLanguage=1;
	NSString *path;
	if(selectedLanguage==1)
		path = [[NSBundle mainBundle] pathForResource:@"English" ofType:@"lproj"];
	else if(selectedLanguage==2)
		path = [[NSBundle mainBundle] pathForResource:@"zh" ofType:@"lproj"];
    
	NSBundle* languageBundle = [NSBundle bundleWithPath:path];
	NSString* str=[languageBundle localizedStringForKey:key value:@"" table:nil];
	return str;
}


-(void)enteredBackground:(NSNotification *)notification {
    [audioPlayer stop];
    audioPlayer=nil;
    playPauseButton.enabled = NO;
    selectedIndexPath = -1;
    [tableView reloadData];
    
}


- (void)viewDidLoad
{
   [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(enteredBackground:)
                                                 name:UIApplicationDidEnterBackgroundNotification
                                               object:nil];
    self.navigationController.navigationBar.topItem.title = @"العودة";

    UIBarButtonItem *helpBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:[self languageSelectedStringForKeyArabic:@"Help"] style:UIBarButtonItemStyleBordered target:self action:@selector(helpButtonPressed:)];
    self.navigationItem.rightBarButtonItem = helpBarButtonItem;
    
    
    languageSharedManager = [LanguageManager sharedManager];
    
    archivesDB=[[PhotoArchivesDataManager alloc] init];
    dropDownViewArray=[[NSMutableArray alloc] init];
    searchArray = [[NSMutableArray alloc] init];
//   [self setView];
    
    
    mpVolumeViewParentView.backgroundColor = [UIColor clearColor];
    MPVolumeView *myVolumeView =[[MPVolumeView alloc] initWithFrame: mpVolumeViewParentView.bounds];
    [mpVolumeViewParentView addSubview: myVolumeView];

    
    
    
    NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:@"http://apps.na.ae/ncdr/ncdr-app/NCDRApp/News/AudioLibrary/default.png"]];
    if (imageData) {
        self.placeholder  =[UIImage imageWithData:imageData];
    }else {
        self.placeholder = [UIImage imageNamed:@"placeholder.png"];
        self.placeholder = _placeholder;
    }


}

-(void) startLoadingData
{
   //if(IS_IPAD)
  //  [self addGesture];
    //searchArray=[archivesDB listOfArchives];
  //  [tableView reloadData];
  //  [self.view viewWithTag:101].hidden=YES;

 //   [[self.view viewWithTag:101] removeFromSuperview];
}

-(void) setView
{

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)openSearchResult:(id)sender
{
    
        //if(searchTextField.text.length>0)
        //{
            [searchTextField resignFirstResponder];
            [self makeResultsFromFiltersAndDatesSelected];
            
        //}
    
}
#pragma mark UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (searchTextField == textField) {
        [textField resignFirstResponder];
        [self openSearchResult:nil];
        return NO;
    }
    
    return YES;
}
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    [self openSearchResult:nil];
    

    return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    [self openSearchResult:nil];

    return YES;
}
#pragma mark touches and gesture events

-(void)addGesture
{
   
    UISwipeGestureRecognizer *swipeGestureR = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeGestureRight:)];
    [swipeGestureR setDirection: UISwipeGestureRecognizerDirectionRight ];
    [self.view addGestureRecognizer:swipeGestureR];
    
    UISwipeGestureRecognizer *swipeGestureL = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeGestureLeft:)];
    [swipeGestureL setDirection: UISwipeGestureRecognizerDirectionLeft];
    [self.view addGestureRecognizer:swipeGestureL];
   if(IS_IPAD)
   {
    UIPinchGestureRecognizer *pinch=[[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(onDoubleTapTextView:)];
    [self.view addGestureRecognizer:pinch];
   }
}


-(void) handleSwipeGestureRight:(UISwipeGestureRecognizer *) sender
{
    NSLog(@"right %i",currentIndex);
    
    if(currentIndex<([searchArray count]-1))
    {
        currentIndex++;
    }
    
}

-(void) handleSwipeGestureLeft:(UISwipeGestureRecognizer *) sender
{
    NSLog(@"left %i",currentIndex);
    
    if(currentIndex>0)
    {
        currentIndex--;
    }
   
    
}

-(IBAction)APlus:(id)sender
{
    if(fontSize<40)
    {
        fontSize++;
    }
}

-(IBAction)AMinus:(id)sender
{
    if(fontSize>20)
    {
        fontSize--;
    }
}

-(void)onDoubleTapTextView:(id)sender
{
    UIPinchGestureRecognizer *pinch=(UIPinchGestureRecognizer*)sender;
    if(pinch.scale>1)
        [self APlus:sender];
    else
        [self AMinus:sender];
}


-(void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [DropDownButton dismissButtons:dropDownViewArray callingButton:nil];
}

-(void)goBack:(id) sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

-(IBAction) goBackToList:(id)sender
{
    tableView.hidden=NO;
}


#pragma mark browse method
-(void) makeFilterComponentsForSearch
{
    
    UIImage *image=[UIImage imageNamed:@"iPhone5 Larg filter_sub 1.png"];
    UIImage *image2=[UIImage imageNamed:@"iPhone5 Samll filter_sub 1.png"];
    //        ////place///
//        NSMutableSet *placesSet = [[NSMutableSet alloc]init];
//        for (NSString *places in [archivesDB listofPhotoPlacesArabic]) {
//            NSArray *items = [places componentsSeparatedByString:@","];
//            for (NSString *place in items) {
//                [placesSet addObject:place];
//            }
//        }
//        placesDropdownArray = [[NSMutableArray alloc] initWithArray:[placesSet allObjects]];
//        [placesDropdownArray insertObject:@"الكل" atIndex:0];
//        [placeDropDownButton initButtonWithData:placesDropdownArray parentTarget:self toView:self.view cellBackground:image];
//        
        ////category///
        NSMutableSet *categoriesSet = [[NSMutableSet alloc]init];
        for (NSString *categories in [archivesDB listofPhotoCategoriesArabic]) {
            NSArray *items = [categories componentsSeparatedByString:@","];
            for (NSString *category in items) {
                [categoriesSet addObject:category];
            }
        }
        categoriesDropDownArray = [[NSMutableArray alloc] initWithArray:[categoriesSet allObjects]];
        [categoriesDropDownArray insertObject:@"الكل" atIndex:0];
        [categoryDropDownButton initButtonWithData:categoriesDropDownArray parentTarget:self toView:self.view cellBackground:image];
        
        ////dateTo///
       // datesDropDownArray = [[NSMutableArray alloc]init];
        //datesDropDownArray = [[NSMutableArray alloc]init];
//        NSMutableSet *datesSet = [[NSMutableSet alloc]init];
//        
//        for (NSDictionary *dic in [archivesDB listofPhotoDates]) {
//            [datesSet addObject:[dic objectForKey:@"YearFrom"]];
//            //[datesSet addObject:[dic objectForKey:@"YearTo"]];
//            
//        }
        NSArray *sortedArray = [[archivesDB listofPhotoDates] sortedArrayUsingComparator: ^(id firstString, id secondString) {
            return [(NSString *)secondString compare:(NSString *)firstString  options:NSNumericSearch];
        }];
        datesDropDownArray = [[NSMutableArray alloc] initWithArray: sortedArray];

        [datesDropDownArray insertObject:@"الكل" atIndex:0];
        
        [fromYearDropDownButton initButtonWithData:datesDropDownArray parentTarget:self toView:self.view cellBackground:image2];
        [toYearDropDownButton initButtonWithData:datesDropDownArray parentTarget:self toView:self.view cellBackground:image2];
    
   

      //////////////////////////////////////////////////////////////////////////////////////////

   // [dropDownViewArray addObject:placeDropDownButton];
    [dropDownViewArray addObject:categoryDropDownButton];
    [dropDownViewArray addObject:fromYearDropDownButton];
    [dropDownViewArray addObject:toYearDropDownButton];
    
}
NSInteger ticketSort(id ticket1, id ticket2, void *context) {
	return [ticket2 compare:ticket1 options:NSNumericSearch];
}
#pragma mark MENU PROTOCOL

-(void) indexClickedForDropDownMenuIndex:(int) index andDropDown:(id)dropDown;
{
    [self goBackToList:nil];
    
//    if(dropDown==placeDropDownButton)
//    {
//        if(IS_IPAD)
//        {
//            if (languageChange==0) {
//                [placeDropDownButton setBackgroundImage:[UIImage imageNamed:@"filter E.png"] forState:UIControlStateNormal];
//
//            }
//            else{
//                [placeDropDownButton setBackgroundImage:[UIImage imageNamed:@"filter A.png"] forState:UIControlStateNormal];
//
//            }
//        }
//        else
//        {
//            if (languageChange==0) {
//                [placeDropDownButton setBackgroundImage:[UIImage imageNamed:@"filter E.png"] forState:UIControlStateNormal];
//                
//            }
//            else{
//                [placeDropDownButton setBackgroundImage:[UIImage imageNamed:@"filter A.png"] forState:UIControlStateNormal];
//                
//            }        }
//        [placeDropDownButton setTitle:[placesDropdownArray objectAtIndex:index] forState:UIControlStateNormal];
//    }
    if(dropDown==categoryDropDownButton)
    {
        
        if(index == 0) {

            [categoryDropDownButton setBackgroundImage:[UIImage imageNamed:@"iPhone5 Larg filter.png"] forState:UIControlStateNormal];
            [categoryDropDownButton setTitle:@"الموضوع" forState:UIControlStateNormal];
           
            
        } else {
            
            [categoryDropDownButton setBackgroundImage:[UIImage imageNamed:@"iPhone5 Larg filter.png"] forState:UIControlStateNormal];
            [categoryDropDownButton setTitle:[categoriesDropDownArray objectAtIndex:index] forState:UIControlStateNormal];
          
            
        }
    }
//    if(dropDown==fromYearDropDownButton)
//    {
//        [fromYearDropDownButton setBackgroundImage:[UIImage imageNamed:@"iPhone5 Small filter.png"] forState:UIControlStateNormal];
//
//        [fromYearDropDownButton setTitle:[datesDropDownArray objectAtIndex:index] forState:UIControlStateNormal];
//    }
    if(dropDown==toYearDropDownButton)
    {
        
        if(index == 0) {
            
            [toYearDropDownButton setBackgroundImage:[UIImage imageNamed:@"iPhone5 Year filter.png"] forState:UIControlStateNormal];
            [toYearDropDownButton setTitle:@"إلى عام" forState:UIControlStateNormal];
            
        } else {
            

            [toYearDropDownButton setBackgroundImage:[UIImage imageNamed:@"iPhone5 Year filter.png"] forState:UIControlStateNormal];
            [toYearDropDownButton setTitle:[datesDropDownArray objectAtIndex:index] forState:UIControlStateNormal];

            
        }

        
        
    }
    if(dropDown==fromYearDropDownButton)
    {
        
        if(index == 0) {
            
            [fromYearDropDownButton setBackgroundImage:[UIImage imageNamed:@"iPhone5 Year filter.png"] forState:UIControlStateNormal];
            [fromYearDropDownButton setTitle:@"من عام" forState:UIControlStateNormal];

            
        } else {
            
            
            [fromYearDropDownButton setBackgroundImage:[UIImage imageNamed:@"iPhone5 Year filter.png"] forState:UIControlStateNormal];
            [fromYearDropDownButton setTitle:[datesDropDownArray objectAtIndex:index] forState:UIControlStateNormal];
            

        }
        
        
        
    }

    [self openSearchResult:nil];
    
}
- (IBAction)helpButtonPressed:(id)sender {
   
 
     //   [helpScreen setBackgroundImage:[UIImage imageNamed:@"iPhone5 home page help.png"] forState:UIControlStateNormal];
        
    if(helpScreen.hidden) {
        helpScreen.hidden = NO;
    } else {
        helpScreen.hidden = YES;
    }
    
}
- (IBAction)dismissHelpPressed:(id)sender {
    helpScreen.hidden = YES;
}

#pragma mark rotation

- (BOOL)shouldAutorotate {
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}
// pre-iOS 6 support
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation {
    return (toInterfaceOrientation == UIInterfaceOrientationPortrait);
}

-(void)makeResultsFromFiltersAndDatesSelected
{
    

   // [self.view viewWithTag:101].hidden=NO;

    if (languageChange==0) {
        
//        searchArray= [archivesDB archivesEnglishFromDate:fromYearDropDownButton.titleLabel.text toDate:toYearDropDownButton.titleLabel.text inCategoryE:categoryDropDownButton.titleLabel.text withKeywordE:searchTextField.text atPlaceE:placeDropDownButton.titleLabel.text];
        
        NSString *categoryString = (categoryDropDownButton.titleLabel.text.length == 0) ? [self languageSelectedStringForKey:@"الموضوع"] :categoryDropDownButton.titleLabel.text;
        NSString *yearString = (toYearDropDownButton.titleLabel.text.length == 0) ? [self languageSelectedStringForKeyArabic:@"End Date"] :toYearDropDownButton.titleLabel.text;
        NSString *yearStringFrom = (fromYearDropDownButton.titleLabel.text.length == 0) ? [self languageSelectedStringForKeyArabic:@"Start Date"] :fromYearDropDownButton.titleLabel.text;

       // searchArray = [archivesDB archivesArabicWithCategoryA:categoryString withKeywordA:searchTextField.text toDate:yearString andFromDate:fromYearDropDownButton.titleLabel.text];
        searchArray = [archivesDB archivesArabicWithCategoryA:categoryDropDownButton.titleLabel.text withKeywordA:searchTextField.text toDate:toYearDropDownButton.titleLabel.text andFromDate:fromYearDropDownButton.titleLabel.text];


    } else {
//                searchArray= [archivesDB archivesArabicFromDate:fromYearDropDownButton.titleLabel.text toDate:toYearDropDownButton.titleLabel.text inCategoryA:categoryDropDownButton.titleLabel.text withKeywordA:searchTextField.text atPlaceA:placeDropDownButton.titleLabel.text ];
        
        NSString *categoryString = (categoryDropDownButton.titleLabel.text.length == 0) ? [self languageSelectedStringForKey:@"الموضوع"] :categoryDropDownButton.titleLabel.text;
        NSString *yearString = (toYearDropDownButton.titleLabel.text.length == 0) ? [self languageSelectedStringForKeyArabic:@"End Date"] :toYearDropDownButton.titleLabel.text;
        NSString *yearStringFrom = (fromYearDropDownButton.titleLabel.text.length == 0) ? [self languageSelectedStringForKeyArabic:@"Start Date"] :fromYearDropDownButton.titleLabel.text;

        //searchArray = [archivesDB archivesArabicWithCategoryA:categoryString withKeywordA:searchTextField.text toDate:yearString andFromDate:fromYearDropDownButton.titleLabel.text];
        searchArray = [archivesDB archivesArabicWithCategoryA:categoryDropDownButton.titleLabel.text withKeywordA:searchTextField.text toDate:toYearDropDownButton.titleLabel.text andFromDate:fromYearDropDownButton.titleLabel.text];


    }
 
//        ArchivesSearchResultsViewController *searchResultsViewController = [[ArchivesSearchResultsViewController alloc] initWithNibName:@"ArchivesSearchResultsViewController" bundle:nil];
//        searchResultsViewController.searchResultsArray = searchArray;
//        [self.navigationController pushViewController:searchResultsViewController animated:YES];
    NSLog(@"count: %d",searchArray.count);
    if (searchArray.count>0) {
        self.playAllButton.enabled = YES;
        
    }
    if (IS_IPHONE) {
        if (searchArray.count<=0) {
            toYearDropDownButton.enabled=YES;
            fromYearDropDownButton.enabled=YES;
            categoryDropDownButton.enabled=YES;
          //  playAllButton.enabled = NO;
            // placeDropDownButton.enabled=NO;
        }
        else{
            toYearDropDownButton.enabled=YES;
            fromYearDropDownButton.enabled=YES;
            categoryDropDownButton.enabled=YES;
            if (searchArray.count>0) {
               // playAllButton.enabled = YES;
                
            }            // placeDropDownButton.enabled=YES;
        }
    }

    [tableView reloadData];
    
  
   
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    //#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    //#warning Incomplete method implementation.
    // Return the number of rows in the section.
    return [searchArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    CustomCellHome *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[CustomCellHome alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
    AudioArchive *photoArchive =[searchArray objectAtIndex:indexPath.row];
    
    cell.backgroundColor = [UIColor clearColor];
    //    NSString *stringName=photoArchive.imageFile;
    //    NSString * str=SERVER_PATH_GALLERY;
    //    NSMutableString *str1 = [stringName mutableCopy];
    //    [str1 insertString:@"G" atIndex:9];
    //    NSString *fullStr=[NSString stringWithFormat:@"%@%@",str,str1];
    NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:
                                                       [NSString stringWithFormat:@"http://apps.na.ae/ncdr/ncdr-app/NCDRApp/News/AudioLibrary/%@", photoArchive.iconFile]]];
    if (imageData) {
        cell.imageView.image  =[UIImage imageWithData:imageData];//[UIImage imageNamed:[NSString stringWithFormat:@"%@",photoArchive.relatedPhoto]];// [UIImage imageWithData:imageData];
    }else {
        cell.imageView.image = _placeholder;
    }
    
    
    cell.backgroundView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"row_n.png"]];
    cell.selectedBackgroundView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"selection pause.png"]];
    
    if (languageChange==0) {//english
        cell.detailTextLabel.text=photoArchive.audioTitle;
        cell.textLabel.text=[NSString stringWithFormat:@"Category:%@",photoArchive.audioSubject] ;
        
        cell.detailTextLabel.textAlignment=UITextAlignmentLeft;
        cell.textLabel.textAlignment=UITextAlignmentLeft;
        cell.textLabel.numberOfLines=1;
        cell.detailTextLabel.numberOfLines=1;
        cell.textLabel.font=[UIFont fontWithName:@"Ariel" size:11];
        cell.detailTextLabel.font=[UIFont fontWithName:@"Ariel" size:11];
        cell.detailTextLabel.lineBreakMode = UILineBreakModeTailTruncation;
        cell.textLabel.lineBreakMode = UILineBreakModeTailTruncation;
    }
    else{//arabic
        cell.detailTextLabel.text=photoArchive.audioTitle;
        //        NSString *soundFilePath = [[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"%@",photoArchive.audioFile ]
        //                                                                  ofType:@"mp3"];
        //        NSURL *soundFileURL = [NSURL fileURLWithPath:soundFilePath];
        NSURL *soundFileURL = [NSURL fileURLWithPath:[photoArchive.audioFile stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
        NSData *_objectData = [NSData dataWithContentsOfURL:soundFileURL];
        NSError *error;
        
        
        AVURLAsset* audioAsset = [AVURLAsset URLAssetWithURL:soundFileURL options:nil];
        CMTime audioDuration = audioAsset.duration;
        float audioDurationSeconds = CMTimeGetSeconds(audioDuration);
        
        cell.detailTextLabel.text=photoArchive.audioTitle;
        cell.textLabel.text=[NSString stringWithFormat:@"الموضوع:%@",photoArchive.audioSubject];
        
        
        cell.detailTextLabel.textAlignment=UITextAlignmentRight;
        cell.textLabel.textAlignment=UITextAlignmentRight;
        cell.textLabel.numberOfLines=1;
        cell.detailTextLabel.numberOfLines=1;
        cell.detailTextLabel.lineBreakMode = UILineBreakModeTailTruncation;
        cell.textLabel.lineBreakMode = UILineBreakModeTailTruncation;
        
        if (IS_IPHONE) {
            cell.textLabel.textColor = [UIColor whiteColor];
            cell.detailTextLabel.textColor = [UIColor whiteColor];
            
        }
        
    }
    if (indexPath.row==selectedIndexPath) {
        // [cell setHighlighted:YES];
        if (!isPause) {
            cell.backgroundView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"selection pause.png"]];
            
        }
        else{
            cell.backgroundView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"selection play.png"]];
            
        }
    }
    else {
        // [cell setHighlighted:NO];
    }

    

    
    return cell;
}


#pragma mark - Table view delegate


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    playAll=NO;
    //  [activityIndicator startAnimating];
    
    if (indexPath.row==selectedIndexPath) {
        playPauseButton.enabled = YES;
        
        if (isPause) {
            [audioPlayer play];
            [playPauseButton setImage:[UIImage imageNamed:@"pause.png"] forState:UIControlStateNormal];
            isPause=NO;
            
        }
        else{
            [audioPlayer pause];
            isPause=YES;
            [playPauseButton setImage:[UIImage imageNamed:@"play.png"] forState:UIControlStateNormal];
            
        }
        
        
    }
    else{
        if (audioPlayer.isPlaying) {
            [audioPlayer stop];
        }
        selectedIndexPath = indexPath.row;
        isPause = NO;
        AudioArchive *photoArchive =[searchArray objectAtIndex:indexPath.row];
        //http://apps.na.ae/ncdr/ncdr-app/NCDRApp/AudioLibrary/Track%201.mp3
        //     NSString *soundFilePath = [[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"%@",photoArchive.audioFile ] ofType:@"mp3"];
        // NSURL *soundFileURL = [NSURL fileURLWithPath:[photoArchive.audioFile stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
        NSURL *soundFileURL = [NSURL URLWithString:[photoArchive.audioFile stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
        //  http://apps.na.ae/ncdr/ncdr-app/NCDRApp/News/Track_1.mp3
        //http://songs.apniisp.com/Strings%20-%20Dhaani/03%20-%20Kahani%20Mohabat%20Ki%20(Apniisp.Com).mp3
        [self performSelectorInBackground:@selector(showActivityIndicator) withObject:nil];

        NSData *_objectData = [NSData dataWithContentsOfURL:[NSURL URLWithString: [NSString stringWithFormat:@"http://apps.na.ae/ncdr/ncdr-app/NCDRApp/News/AudioLibrary/%@",photoArchive.audioFile ]]];
        NSError *error;
        [self performSelectorInBackground:@selector(hideActivityIndicator) withObject:nil];
        audioPlayer = [[AVAudioPlayer alloc]initWithData:_objectData error:&error ];// initWithContentsOfURL:soundFileURL error:nil];
        audioPlayer.delegate = self;
        audioPlayer.numberOfLoops = 0;

        if (audioPlayer == nil)
            NSLog([error description]);
        playerSlider.maximumValue = [audioPlayer duration];
        playerSlider.value = 0.0;
        
        [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updateTime:) userInfo:nil repeats:YES];
        [audioPlayer play];
        playPauseButton.enabled = YES;
        [playPauseButton setImage:[UIImage imageNamed:@"pause.png"] forState:UIControlStateNormal];
        
    }
    
    [tableView reloadData];
    // [activityIndicator stopAnimating];
    
}

//- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object
//                        change:(NSDictionary *)change context:(void *)context {
//    if (object == audioPlayer && [keyPath isEqualToString:@"status"]) {
//        if (audioPlayer.status == AVPlayerStatusReadyToPlay) {
//            //DISABLE THE UIACTIVITY INDICATOR HERE
//        } else if (audioPlayer.status == AVPlayerStatusFailed) {
//            // something went wrong. player.error should contain some information
//        }
//    }
//}

#pragma mark - Playing Audio

- (void) playSelected:(id) sender;
{
    NSLog(@"Play song number %d", [sender tag]);
    AudioArchive *photoArchive =[searchArray objectAtIndex:[sender tag]];
    
    //    NSString *soundFilePath = [[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"%@",photoArchive.audioFile ]
    //                                                              ofType:@"mp3"];
    //    NSURL *soundFileURL = [NSURL fileURLWithPath:soundFilePath];
    NSURL *soundFileURL = [NSURL fileURLWithPath:[photoArchive.audioFile stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    
    [self performSelectorInBackground:@selector(showActivityIndicator) withObject:nil];

    NSData *_objectData = [NSData dataWithContentsOfURL:[NSURL URLWithString: [NSString stringWithFormat:@"http://apps.na.ae/ncdr/ncdr-app/NCDRApp/News/AudioLibrary/%@",photoArchive.audioFile ]]];
    
    [self performSelectorInBackground:@selector(hideActivityIndicator) withObject:nil];

    NSError *error;
    
    audioPlayer = [[AVAudioPlayer alloc]initWithData:_objectData error:&error ];// initWithContentsOfURL:soundFileURL error:nil];
    
    audioPlayer.delegate = self;
    audioPlayer.numberOfLoops = 0;
    
    playerSlider.maximumValue = [audioPlayer duration];
    playerSlider.value = 0.0;
    
    [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updateTime:) userInfo:nil repeats:YES];
    [audioPlayer play];
    
}

- (IBAction)slide {
    audioPlayer.currentTime = playerSlider.value;
}

- (void)updateTime:(NSTimer *)timer {
    playerSlider.value = audioPlayer.currentTime;
}


- (IBAction)playAllPressed:(id)sender {
    if (searchArray.count>0) {
        playAll = YES;
        //   [activityIndicator startAnimating];
        
        if (audioPlayer.isPlaying) {
            [audioPlayer stop];
        }
        isPause = NO;
        selectedIndexPath = 0;
        AudioArchive *photoArchive =[searchArray objectAtIndex:selectedIndexPath];
        
        [self performSelectorInBackground:@selector(showActivityIndicator) withObject:nil];

        NSData *_objectData = [NSData dataWithContentsOfURL:[NSURL URLWithString: [NSString stringWithFormat:@"http://apps.na.ae/ncdr/ncdr-app/NCDRApp/News/AudioLibrary/%@",photoArchive.audioFile ]]];
        [self performSelectorInBackground:@selector(hideActivityIndicator) withObject:nil];

        
        NSError *error;
        
        audioPlayer = [[AVAudioPlayer alloc]initWithData:_objectData error:&error ];// initWithContentsOfURL:soundFileURL error:nil];
        audioPlayer.delegate = self;
        audioPlayer.numberOfLoops = 0;
        playerSlider.maximumValue = [audioPlayer duration];
        playerSlider.value = 0.0;
        
        [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updateTime:) userInfo:nil repeats:YES];
        playAll = YES;
        
        [audioPlayer play];
        playPauseButton.enabled = YES;
        [playPauseButton setImage:[UIImage imageNamed:@"pause.png"] forState:UIControlStateNormal];
        [tableView reloadData];
    }
    
    
    
}

- (void)showActivityIndicator
{
    if ([NSThread isMainThread])
        NSLog(@"Is Main");
    else
        NSLog(@"is background");
    activityView.hidden = NO;
}

- (void)hideActivityIndicator
{
    activityView.hidden = YES;
}
#pragma mark - AUdio Player

- (void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag{
    if (playAll) {
      //  [activityIndicator startAnimating];
        
        isPause = NO;
        selectedIndexPath = selectedIndexPath+1;
        AudioArchive *photoArchive =[searchArray objectAtIndex:selectedIndexPath];
        
        
        NSURL *soundFileURL = [NSURL fileURLWithPath:[photoArchive.audioFile stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
        [self performSelectorInBackground:@selector(showActivityIndicator) withObject:nil];

        NSData *_objectData = [NSData dataWithContentsOfURL:[NSURL URLWithString: [NSString stringWithFormat:@"http://apps.na.ae/ncdr/ncdr-app/NCDRApp/News/AudioLibrary/%@",photoArchive.audioFile ]]];
        [self performSelectorInBackground:@selector(hideActivityIndicator) withObject:nil];

        
        NSError *error;

        audioPlayer = [[AVAudioPlayer alloc]initWithData:_objectData error:&error ];// initWithContentsOfURL:soundFileURL error:nil];
        audioPlayer.delegate = self;
        audioPlayer.numberOfLoops = 0;
        
        playerSlider.maximumValue = [audioPlayer duration];
        playerSlider.value = 0.0;
        
        [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updateTime:) userInfo:nil repeats:YES];
        [audioPlayer play];
        playPauseButton.enabled = YES;
        [playPauseButton setImage:[UIImage imageNamed:@"pause.png"] forState:UIControlStateNormal];
        [tableView reloadData];
      //  [activityIndicator stopAnimating];
        
    }
    
    
}
- (IBAction)playPausePressed:(id)sender {
    
    //  UIButton *btn = (UIButton *)sender;
    
    if (audioPlayer.isPlaying) {
        [audioPlayer pause];
        [playPauseButton setImage:[UIImage imageNamed:@"play.png"] forState:UIControlStateNormal];
        isPause = YES;
        
    }
    else{
        [audioPlayer play];
        [playPauseButton setImage:[UIImage imageNamed:@"pause.png"] forState:UIControlStateNormal];
        isPause = NO;
        
    }
    [tableView reloadData];
}

- (IBAction)moreButtonPressed:(id)sender {
    
        MoreAppsViewController *searchResultsViewController = [[MoreAppsViewController alloc] initWithNibName:@"MoreAppsViewController" bundle:nil];
    self.navigationItem.title = @"العودة";

        [self.navigationController pushViewController:searchResultsViewController animated:YES];
    
    
}




@end
