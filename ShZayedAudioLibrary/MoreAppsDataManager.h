//
//  BookDatabaseManager.h
//  Yomiyat
//
//  Created by Aisha on 5/4/13.
//  Copyright (c) 2013 Aisha. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DBManager.h"

@interface MoreAppsDataManager : NSObject<SelectOperationDelegate>
{
    DBManager *dbManager;
    NSMutableArray *appsArray;


    int selectQuery;

    
}
@property(strong,nonatomic)NSMutableArray *appsArray;

// English
-(NSMutableArray *) listOfArchives;
-(NSMutableArray *) archiveEnglishForId:(int) index;


@end
