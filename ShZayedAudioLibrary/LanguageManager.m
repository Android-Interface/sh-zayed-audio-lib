//
//  LanguageManager.m
//  FSVerticalTabBarExample
//
//  Created by Aisha on 1/7/13.
//
//

#import "LanguageManager.h"

@implementation LanguageManager
@synthesize selectedLanguage=selectedLanguage_;
#define ENGLISH_LANGUAGE 1
#define ARABIC_LANGUAGE 2
#pragma mark Singleton Methods
static LanguageManager *sharedMyManager;
+ (id)sharedManager {
    @synchronized(self) {
        if (sharedMyManager == nil)
            sharedMyManager = [[self alloc] init];
    }
    return sharedMyManager;
}
- (id)init
{
    if (self = [super init]) {
        selectedLanguage_=ARABIC_LANGUAGE;
      //  someProperty = [[NSString alloc] initWithString:@"Default Property Value"];
    }
    return self;
}
////
//-(NSString*) languageSelectedStringForKeyArabic:(NSString*) key
//{
//    // [self test];
//    int  selectedLanguage=2;
//	NSString *path;
//	if(selectedLanguage==1)
//		path = [[NSBundle mainBundle] pathForResource:@"English" ofType:@"lproj"];
//	else if(selectedLanguage==2)
//		path = [[NSBundle mainBundle] pathForResource:@"zh" ofType:@"lproj"];
//    
//	NSBundle* languageBundle = [NSBundle bundleWithPath:path];
//	NSString* str=[languageBundle localizedStringForKey:key value:@"" table:nil];
//	return str;
//}
////
-(NSString*) languageSelectedStringForKey:(NSString*) key
{
	NSString *path;
	if(selectedLanguage_==ENGLISH_LANGUAGE)
		path = [[NSBundle mainBundle] pathForResource:@"English" ofType:@"lproj"];
	else if(selectedLanguage_==ARABIC_LANGUAGE)
		path = [[NSBundle mainBundle] pathForResource:@"zh" ofType:@"lproj"];
    
	NSBundle* languageBundle = [NSBundle bundleWithPath:path];
	NSString* str=[languageBundle localizedStringForKey:key value:@"" table:nil];
	return str;
}


- (void)dealloc {
    // Should never be called, but just here for clarity really.
}

@end
