//
//  ArchivesSearchResultsViewController.m
//  FSVerticalTabBarExample
//
//  Created by Anam on 2/8/14.
//
//

#import "ArchivesSearchResultsViewController.h"
#import "CustomCell.h"
#import "AudioArchive.h"
#import <AVFoundation/AVFoundation.h>
@interface ArchivesSearchResultsViewController ()

@end

@implementation ArchivesSearchResultsViewController
@synthesize searchResultsArray,searchTableView;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        searchResultsArray = [[NSMutableArray alloc] init];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
//    self.navigationController.navigationBarHidden = NO;
//    self.navigationController.title = @"Advanced Search";
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(enteredBackground:)
                                                 name:UIApplicationDidEnterBackgroundNotification
                                               object:nil];
    NSString *soundFilePath = [[NSBundle mainBundle] pathForResource:@"Track 1"
                                                              ofType:@"mp3"];
    NSURL *soundFileURL = [NSURL fileURLWithPath:soundFilePath];
    audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:soundFileURL error:nil];
    audioPlayer.numberOfLoops = 0;
    [audioPlayer prepareToPlay];
    mpVolumeViewParentView.backgroundColor = [UIColor clearColor];
    MPVolumeView *myVolumeView =[[MPVolumeView alloc] initWithFrame: mpVolumeViewParentView.bounds];
    [mpVolumeViewParentView addSubview: myVolumeView];
    // Do any additional setup after loading the view from its nib.
}
-(void)enteredBackground:(NSNotification *)notification {
    [audioPlayer stop];
    audioPlayer=nil;
    playPauseButton.enabled = NO;
    selectedIndexPath = -1;
    [searchTableView reloadData];
    
}
-(void) viewWillDisappear:(BOOL)animated
{
    [audioPlayer stop];
    audioPlayer=nil;
}
-(void) viewDidDisappear:(BOOL)animated
{
    [audioPlayer stop];
    audioPlayer=nil;
}
-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden= YES;

        playPauseButton.enabled = NO;
    selectedIndexPath = -1;
        // [self performSelector:@selector(startLoadingData) withObject:nil afterDelay:0.5];
    
}
- (void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag{
    if (playAll) {
        isPause = NO;
        selectedIndexPath = selectedIndexPath+1;
        AudioArchive *photoArchive =[searchResultsArray objectAtIndex:selectedIndexPath];
        
        NSURL *soundFileURL = [NSURL fileURLWithPath:[photoArchive.audioFile stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
        
        NSData *_objectData = [NSData dataWithContentsOfURL:soundFileURL];
        NSError *error;
        
        audioPlayer = [[AVAudioPlayer alloc]initWithData:_objectData error:&error ];// initWithContentsOfURL:soundFileURL error:nil];        audioPlayer.delegate = self;

        audioPlayer.delegate = self;
        audioPlayer.numberOfLoops = 0;
        
        playerSlider.maximumValue = [audioPlayer duration];
        playerSlider.value = 0.0;
        
        [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updateTime:) userInfo:nil repeats:YES];
        [audioPlayer play];
        playPauseButton.enabled = YES;
        [playPauseButton setImage:[UIImage imageNamed:@"pause.png"] forState:UIControlStateNormal];
        [searchTableView reloadData];
    }
    
    
}
- (IBAction)playPausePressed:(id)sender {
    
    //  UIButton *btn = (UIButton *)sender;
    
    if (audioPlayer.isPlaying) {
        [audioPlayer pause];
        [playPauseButton setImage:[UIImage imageNamed:@"play.png"] forState:UIControlStateNormal];
        isPause = YES;
        
    }
    else{
        [audioPlayer play];
        [playPauseButton setImage:[UIImage imageNamed:@"pause.png"] forState:UIControlStateNormal];
        isPause = NO;
        
    }
    [searchTableView reloadData];
}
- (IBAction)playAllPressed:(id)sender {
    playAll = YES;
    if (audioPlayer.isPlaying) {
        [audioPlayer stop];
    }
    isPause = NO;
    selectedIndexPath = 0;
    AudioArchive *photoArchive =[searchResultsArray objectAtIndex:selectedIndexPath];
    NSURL *soundFileURL = [NSURL fileURLWithPath:[photoArchive.audioFile stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    
    NSData *_objectData = [NSData dataWithContentsOfURL:soundFileURL];
    NSError *error;
    
    audioPlayer = [[AVAudioPlayer alloc]initWithData:_objectData error:&error ];// initWithContentsOfURL:soundFileURL error:nil];        audioPlayer.delegate = self;

    audioPlayer.delegate = self;
    audioPlayer.numberOfLoops = 0;
    
    playerSlider.maximumValue = [audioPlayer duration];
    playerSlider.value = 0.0;
    
    [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updateTime:) userInfo:nil repeats:YES];
    [audioPlayer play];
    playPauseButton.enabled = YES;
    [playPauseButton setImage:[UIImage imageNamed:@"pause.png"] forState:UIControlStateNormal];
    [searchTableView reloadData];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)backButoonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    //#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    //#warning Incomplete method implementation.
    // Return the number of rows in the section.
    return [searchResultsArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    CustomCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[CustomCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
    AudioArchive *photoArchive =[searchResultsArray objectAtIndex:indexPath.row];
    
    cell.backgroundColor = [UIColor clearColor];
    //    NSString *stringName=photoArchive.imageFile;
    //    NSString * str=SERVER_PATH_GALLERY;
    //    NSMutableString *str1 = [stringName mutableCopy];
    //    [str1 insertString:@"G" atIndex:9];
    //    NSString *fullStr=[NSString stringWithFormat:@"%@%@",str,str1];
    //    NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString: fullStr]];
    NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:
                                                       [NSString stringWithFormat:@"http://apps.na.ae/ncdr/ncdr-app/NCDRApp/News/AudioLibrary/%@", photoArchive.iconFile]]];
    cell.imageView.image  =[UIImage imageWithData:imageData];
    
    cell.backgroundView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"row_n.png"]];
    cell.selectedBackgroundView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"selection play.png"]];
    
    NSURL *soundFileURL = [NSURL fileURLWithPath:[photoArchive.audioFile stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    
    NSData *_objectData = [NSData dataWithContentsOfURL:soundFileURL];
    NSError *error;
    
    audioPlayer = [[AVAudioPlayer alloc]initWithData:_objectData error:&error ];// initWithContentsOfURL:soundFileURL error:nil];        audioPlayer.delegate = self;

//    audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:soundFileURL error:nil];
//    audioPlayer.delegate = self;
//    audioPlayer.numberOfLoops = 0;
    
    AVURLAsset* audioAsset = [AVURLAsset URLAssetWithURL:soundFileURL options:nil];
    CMTime audioDuration = audioAsset.duration;
    float audioDurationSeconds = CMTimeGetSeconds(audioDuration);
    
        cell.detailTextLabel.text=photoArchive.audioTitle;
        cell.textLabel.text=[NSString stringWithFormat:@"الموضوع:%@                              %.2f ",photoArchive.audioSubject,audioPlayer.duration/60];
        
        cell.detailTextLabel.textAlignment=UITextAlignmentRight;
        cell.textLabel.textAlignment=UITextAlignmentRight;
        cell.textLabel.numberOfLines=1;
        cell.detailTextLabel.numberOfLines=1;
        cell.detailTextLabel.lineBreakMode = UILineBreakModeTailTruncation;
        cell.textLabel.lineBreakMode = UILineBreakModeTailTruncation;
        
  
    if (indexPath.row==selectedIndexPath) {
        // [cell setHighlighted:YES];
        if (!isPause) {
            cell.backgroundView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"selection play.png"]];
            
        }
        else{
            cell.backgroundView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"selection pause.png"]];
            
        }
    }
    else {
        // [cell setHighlighted:NO];
    }
    
    return cell;
}

- (void) playSelected:(id) sender;
{
    NSLog(@"Play song number %d", [sender tag]);
    AudioArchive *photoArchive =[searchResultsArray objectAtIndex:[sender tag]];
    
    NSURL *soundFileURL = [NSURL fileURLWithPath:[photoArchive.audioFile stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    
    NSData *_objectData = [NSData dataWithContentsOfURL:soundFileURL];
    NSError *error;
    
    audioPlayer = [[AVAudioPlayer alloc]initWithData:_objectData error:&error ];// initWithContentsOfURL:soundFileURL error:nil];        audioPlayer.delegate = self;

    audioPlayer.delegate = self;
    audioPlayer.numberOfLoops = 0;
    
    playerSlider.maximumValue = [audioPlayer duration];
    playerSlider.value = 0.0;
    
    [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updateTime:) userInfo:nil repeats:YES];
    [audioPlayer play];
    
}

- (IBAction)slide {
    audioPlayer.currentTime = playerSlider.value;
}

- (void)updateTime:(NSTimer *)timer {
    playerSlider.value = audioPlayer.currentTime;
}
#pragma mark - Table view delegate


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    playAll=NO;
    
    if (indexPath.row==selectedIndexPath) {
        playPauseButton.enabled = YES;
        
        if (isPause) {
            [audioPlayer play];
            [playPauseButton setImage:[UIImage imageNamed:@"pause.png"] forState:UIControlStateNormal];
            isPause=NO;
            
        }
        else{
            [audioPlayer pause];
            isPause=YES;
            [playPauseButton setImage:[UIImage imageNamed:@"play.png"] forState:UIControlStateNormal];
            
        }
        
    }
    else{
        if (audioPlayer.isPlaying) {
            [audioPlayer stop];
        }
        selectedIndexPath = indexPath.row;
        isPause = NO;
        AudioArchive *photoArchive =[searchResultsArray objectAtIndex:indexPath.row];
        //http://apps.na.ae/ncdr/ncdr-app/NCDRApp/AudioLibrary/Track%201.mp3
        //     NSString *soundFilePath = [[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"%@",photoArchive.audioFile ] ofType:@"mp3"];
        // NSURL *soundFileURL = [NSURL fileURLWithPath:[photoArchive.audioFile stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
        NSURL *soundFileURL = [NSURL URLWithString:[photoArchive.audioFile stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
        //  http://apps.na.ae/ncdr/ncdr-app/NCDRApp/News/Track_1.mp3
        //http://songs.apniisp.com/Strings%20-%20Dhaani/03%20-%20Kahani%20Mohabat%20Ki%20(Apniisp.Com).mp3
        
        NSData *_objectData = [NSData dataWithContentsOfURL:[NSURL URLWithString: [NSString stringWithFormat:@"http://apps.na.ae/ncdr/ncdr-app/NCDRApp/News/AudioLibrary/%@",photoArchive.audioFile ]]];
        NSError *error;
        
        audioPlayer = [[AVAudioPlayer alloc]initWithData:_objectData error:&error ];// initWithContentsOfURL:soundFileURL error:nil];
        audioPlayer.delegate = self;
        audioPlayer.numberOfLoops = 0;
        if (audioPlayer == nil)
            NSLog([error description]);
        playerSlider.maximumValue = [audioPlayer duration];
        playerSlider.value = 0.0;
        
        [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updateTime:) userInfo:nil repeats:YES];
        [audioPlayer play];
        playPauseButton.enabled = YES;
        [playPauseButton setImage:[UIImage imageNamed:@"pause.png"] forState:UIControlStateNormal];
        
    }
    
    [tableView reloadData];
}

@end
