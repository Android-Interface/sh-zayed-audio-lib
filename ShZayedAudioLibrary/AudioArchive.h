//
//  PhotoArchive.h
//  FSVerticalTabBarExample
//
//  Created by Anam on 2/2/14.
//
//

#import <Foundation/Foundation.h>

@interface AudioArchive : NSObject
{
    int audioID;
    NSString *audioTitle;
    NSString *audioSubject;
    NSString *audioSubject2;
    NSString *audioSubject3;
    NSString *audioFile;
    NSString *iconFile;
    NSString *keywordA;
    NSString *audioDescription;
    NSString *audioDay;
    NSString *audioMonth;
    NSString *audioYear;
    NSString *audioYearTo;
    NSString *audioNotes;

    
    
}


@property(nonatomic,assign)int audioID;
@property(nonatomic,retain)NSString *audioTitle;
@property(nonatomic,retain)NSString *audioSubject;
@property(nonatomic,retain)NSString *audioSubject2;
@property(nonatomic,retain)NSString *audioSubject3;
@property(nonatomic,retain)NSString *audioFile;
@property(nonatomic,retain)NSString *iconFile;
@property(nonatomic,retain)NSString *keywordA;
@property(nonatomic,retain)NSString *audioDescription;
@property(nonatomic,retain)NSString *audioDay;
@property(nonatomic,retain)NSString *audioMonth;
@property(nonatomic,retain)NSString *audioYear;
@property(nonatomic,retain)NSString *audioYearTo;
@property(nonatomic,retain)NSString *audioNotes;
@end
