//
//  DropDownButton.m
//  pickerControl
//
//  Created by Aisha on 5/27/13.
//  Copyright (c) 2013 Aisha. All rights reserved.
//

#import "DropDownButton.h"
//#import "MyUtilities.h"

#define ROW_HEIGHT 37
@implementation DropDownButton
@synthesize isOpen,data,parent,mode;

+(void) dismissButtons:(NSMutableArray*) buttonsArray callingButton:(id)sender
{
    DropDownButton *senderButton;
    senderButton=(DropDownButton*)sender;
    for(int i=0;i<buttonsArray.count;i++)
    {
        DropDownButton *button=[buttonsArray objectAtIndex:i];
        if(senderButton!=button)
        {
            [button dismissControlWhenClickedOutside];
        }
    }
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
      self.titleLabel.lineBreakMode = UILineBreakModeWordWrap;
    }
    return self;
}

- (CGSize)sizeThatFits:(CGSize)size {
    
    // for the width, I subtract 24 for the border
    // for the height, I use a large value that will be reduced when the size is returned from sizeWithFont
    CGSize tempSize = CGSizeMake(size.width - 24, 1000);
    
    CGSize stringSize = [self.titleLabel.text
                         sizeWithFont:self.titleLabel.font
                         constrainedToSize:tempSize
                         lineBreakMode:UILineBreakModeWordWrap];
    
    return CGSizeMake(size.width - 24, stringSize.height);
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/
-(void) initButtonWithData:(NSMutableArray *)array parentTarget:(id)parents toView:(UIView *) parentView cellBackground:(UIImage*) cellBackground
{
        [self addTarget:self action:@selector(openDropDown:) forControlEvents:UIControlEventTouchUpInside];
        parent=parents;
        int height=[array count]*ROW_HEIGHT;
        if(height>185)
        {
            height=185;
        }
    
    
//    for (NSString *str in array) {
//        NSLog(@"downData: %@",str);
//    }
    
        //
        if(IS_IPHONE)
        {
                CGSize result = [[UIScreen mainScreen] bounds].size;
                
                if(result.height == 480)
                {
                     height=111;
                }
            
            height = 370;
        } else {
            
        }
    
    
        //
        tableView=[[UITableView alloc] initWithFrame:CGRectMake(self.frame.origin.x,self.frame.origin.y+self.frame.size.height,self.frame.size.width,height)];
        // view.backgroundColor=[UIColor greenColor];
        tableView.delegate=self;
        tableView.dataSource=self;
        tableView.separatorStyle=UITableViewCellSelectionStyleNone;
        tableView.backgroundColor=[UIColor clearColor];
        tableView.bounces=NO;
        [tableView.layer setCornerRadius:5.0f];
    tableView.layer.borderColor=[UIColor grayColor].CGColor;
    tableView.layer.borderWidth=0.6f;
        [tableView.layer setMasksToBounds:YES];
        [parentView addSubview:tableView];
        tableView.hidden=YES;
        data=array;
}

-(void) openDropDown:(id) sender
{
    //
    [DropDownButton dismissButtons:dropDownViewArray callingButton:sender];
    if(isOpen)
    {
        //close
        tableView.hidden=YES;
        isOpen=NO;
    }
    else
    {
        //open
        tableView.hidden=NO;
        isOpen=YES;
    }

}

-(void) dismissControlWhenClickedOutside
{
    //close
    tableView.hidden=YES;
    isOpen=NO;
}

#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return [data count];
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableViewLocal cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableViewLocal dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    // Configure the cell...
    cell.textLabel.text=[data objectAtIndex:indexPath.row];
    cell.textLabel.textAlignment=UITextAlignmentCenter;
    cell.textLabel.textColor=[UIColor whiteColor];//[UIColor colorWithRed:167.0/255.0 green:62.0/255.0 blue:46.0/255.0 alpha:1.0];
    cell.textLabel.backgroundColor = [UIColor clearColor];
    cell.detailTextLabel.backgroundColor = [UIColor clearColor];
    cell.font=[UIFont fontWithName:@"Ariel" size:8];
    return cell;
}

- (void)tableView: (UITableView*)tableView willDisplayCell: (UITableViewCell*)cell forRowAtIndexPath: (NSIndexPath*)indexPath
{
    UIImage *img = [UIImage imageNamed:@"iPhone5 Larg filter_sub 1.png"];  // get your background image
    UIColor *backgroundColor = [[UIColor alloc] initWithPatternImage:img];
    cell.backgroundColor = backgroundColor;//indexPath.row % 2 ? [UIColor colorWithRed:255/255.0 green:108/255.0 blue:61/255.0 alpha:0.7]
    //  : [UIColor whiteColor];
    cell.textLabel.backgroundColor = [UIColor clearColor];
    cell.detailTextLabel.backgroundColor = [UIColor clearColor];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     [detailViewController release];
     */
  ///  [self openDropDown:nil];
    [self dismissControlWhenClickedOutside];
    [parent indexClickedForDropDownMenuIndex:indexPath.row andDropDown:(DropDownButton*)self];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return ROW_HEIGHT;
}

@end
