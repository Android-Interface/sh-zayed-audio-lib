//
//  SideMenuViewController.h
//  MFSideMenuDemo
//
//  Created by Michael Frederick on 3/19/12.

#import <UIKit/UIKit.h>
#import "MFSideMenu.h"
#import <QuickLook/QuickLook.h>

@interface SideMenuViewController : UITableViewController<QLPreviewControllerDataSource>
{
       NSArray *arrayOfDocuments;
}
@property (nonatomic, assign) MFSideMenu *sideMenu;

@end