//
//  MoreAppsViewController.m
//  ShZayedAudioLibrary
//
//  Created by Anam on 2/12/14.
//  Copyright (c) 2014 Interface. All rights reserved.
//

#import "MoreAppsViewController.h"
#import "CustomCellMore.h"
@interface MoreAppsViewController ()

@end

@implementation MoreAppsViewController
@synthesize tableView;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        moreAppsArray=[[NSMutableArray alloc] init];
        moreAppsDataManager = [[MoreAppsDataManager alloc]init];
        languageSharedManager = [LanguageManager sharedManager];
      
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if(IS_IPHONE) {
        self.navigationController.navigationBar.hidden=NO;
        self.navigationController.navigationBarHidden= NO;
        self.navigationController.navigationBar.backItem.title = @"العودة";

    } else{
        self.navigationController.navigationBarHidden= NO;
    }
    

}
-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if(IS_IPHONE) {
        
        self.navigationController.navigationBar.hidden=NO;
        self.navigationController.navigationBarHidden= NO;
    }
    else{
        self.navigationController.navigationBarHidden= NO;
    }
    
  //  self.navigationController.navigationBar.barStyle = UIBarStyleDefault;
    
    
    if (languageChange==0) {
        self.mainTitleLabel.text = [self languageSelectedStringForKey:@"More Apps"];
        self.navigationItem.title=[self languageSelectedStringForKey:@"More Apps"];

    }
    else{
        self.mainTitleLabel.text = [self languageSelectedStringForKeyArabic:@"More Apps"];
        self.navigationItem.title=[self languageSelectedStringForKeyArabic:@"More Apps"];

    }

   // languageChange=1;
    moreAppsArray = [moreAppsDataManager listOfArchives];
    [tableView reloadData];
}
-(NSString*) languageSelectedStringForKeyArabic:(NSString*) key
{
    // [self test];
    int  selectedLanguage=2;
	NSString *path;
	if(selectedLanguage==1)
		path = [[NSBundle mainBundle] pathForResource:@"English" ofType:@"lproj"];
	else if(selectedLanguage==2)
		path = [[NSBundle mainBundle] pathForResource:@"zh" ofType:@"lproj"];
    
	NSBundle* languageBundle = [NSBundle bundleWithPath:path];
	NSString* str=[languageBundle localizedStringForKey:key value:@"" table:nil];
	return str;
}

-(NSString*) languageSelectedStringForKey:(NSString*) key
{
    // [self test];
    int  selectedLanguage=1;
	NSString *path;
	if(selectedLanguage==1)
		path = [[NSBundle mainBundle] pathForResource:@"English" ofType:@"lproj"];
	else if(selectedLanguage==2)
		path = [[NSBundle mainBundle] pathForResource:@"zh" ofType:@"lproj"];
    
	NSBundle* languageBundle = [NSBundle bundleWithPath:path];
	NSString* str=[languageBundle localizedStringForKey:key value:@"" table:nil];
	return str;
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    //#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    //#warning Incomplete method implementation.
    // Return the number of rows in the section.
    return [moreAppsArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    CustomCellMore *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[CustomCellMore alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
    MoreApp *photoArchive =[moreAppsArray objectAtIndex:indexPath.row];
    
    cell.backgroundColor = [UIColor clearColor];

    cell.imageView.image  =[UIImage imageNamed:[NSString stringWithFormat:@"%@",photoArchive.appIcon]];// [UIImage imageWithData:imageData];
    
    
    
    if (languageChange==0) {//english
        if (IS_IPAD) {
            cell.detailTextLabel.text=photoArchive.appDescriptionEnglish;

        }
        if (IS_IPHONE) {
            cell.textLabel.text=photoArchive.appNameEnglish ;

        }
        
        cell.detailTextLabel.textAlignment=UITextAlignmentLeft;
        cell.textLabel.textAlignment=UITextAlignmentLeft;
        cell.textLabel.numberOfLines=2;
        cell.detailTextLabel.numberOfLines=10;
        cell.textLabel.font=[UIFont boldSystemFontOfSize:12.0];// fontWithName:@"Ariel" size:11];
        cell.detailTextLabel.font=[UIFont systemFontOfSize:10.0];// fontWithName:@"Ariel" size:11];
        cell.detailTextLabel.lineBreakMode = UILineBreakModeTailTruncation;
        cell.textLabel.lineBreakMode = UILineBreakModeTailTruncation;
    }
    else{//arabic
        if (IS_IPAD) {
            cell.detailTextLabel.text=photoArchive.appDescriptionArabic;
            
        }
        if (IS_IPHONE) {
            cell.textLabel.text=photoArchive.appNameArabic ;
            
        }
        cell.detailTextLabel.textAlignment=UITextAlignmentRight;
        cell.textLabel.textAlignment=UITextAlignmentRight;
        cell.textLabel.numberOfLines=2;
        cell.detailTextLabel.numberOfLines=10;
        cell.textLabel.font=[UIFont boldSystemFontOfSize:12.0];// fontWithName:@"Ariel" size:11];
        cell.detailTextLabel.font=[UIFont systemFontOfSize:10.0];// fontWithName:@"Ariel" size:11];
        cell.detailTextLabel.lineBreakMode = UILineBreakModeTailTruncation;
        cell.textLabel.lineBreakMode = UILineBreakModeTailTruncation;
        
        
    }
   
    return cell;
}


#pragma mark - Table view delegate


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    MoreApp *photoArchive =[moreAppsArray objectAtIndex:indexPath.row];

    [self presentAppStoreForID:[NSNumber numberWithInt:photoArchive.appID] withDelegate:self withURL:[NSURL URLWithString:photoArchive.appLink]];
    
}
- (void)presentAppStoreForID:(NSNumber *)appStoreID withDelegate:(id<SKStoreProductViewControllerDelegate>)delegate withURL:(NSURL *)appStoreURL
{
    if(NSClassFromString(@"SKStoreProductViewController")) { // Checks for iOS 6 feature.
        
        SKStoreProductViewController *storeController = [[SKStoreProductViewController alloc] init];
        storeController.delegate = delegate; // productViewControllerDidFinish
        
        // Example app_store_id (e.g. for Words With Friends)
        // [NSNumber numberWithInt:322852954];
        
        NSDictionary *productParameters = @{ SKStoreProductParameterITunesItemIdentifier : appStoreID };
        
        
        [storeController loadProductWithParameters:productParameters completionBlock:^(BOOL result, NSError *error) {
            if (result) {
                [self presentViewController:storeController animated:YES completion:nil];
            } else {
                [[[UIAlertView alloc] initWithTitle:@"Uh oh!" message:@"There was a problem displaying the app" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil] show];
            }
        }];
        
        
    } else { // Before iOS 6, we can only open the URL
        [[UIApplication sharedApplication] openURL:appStoreURL];
    }
}
- (void)productViewControllerDidFinish:(SKStoreProductViewController *)viewController
{
    [viewController dismissViewControllerAnimated:YES completion:nil];
    

}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
