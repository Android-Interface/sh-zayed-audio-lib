//
//  ArchivesSearchResultsViewController.h
//  FSVerticalTabBarExample
//
//  Created by Anam on 2/8/14.
//
//

#import <UIKit/UIKit.h>
#import "LanguageManager.h"

@interface ArchivesSearchResultsViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,AVAudioPlayerDelegate>{
    
    IBOutlet UISlider *playerSlider;
    IBOutlet UIButton *playPauseButton;
    IBOutlet UIView *mpVolumeViewParentView;
    
    AVAudioPlayer *audioPlayer;
    BOOL isPause;
    BOOL playAll;
    int selectedIndexPath;

}
@property (strong, nonatomic) IBOutlet UITableView *searchTableView;
- (IBAction)backButoonPressed:(id)sender;
@property(nonatomic,retain) NSMutableArray *searchResultsArray;

- (IBAction)slide ;
@end
