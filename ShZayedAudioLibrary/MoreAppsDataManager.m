//
//  BookDatabaseManager.m
//  Yomiyat
//
//  Created by Aisha on 5/4/13.
//  Copyright (c) 2013 Aisha. All rights reserved.
//

#import "MoreAppsDataManager.h"
#import "MoreApp.h"
@implementation MoreAppsDataManager
@synthesize appsArray;

- (id)init
{
    self = [super init];
    if (self) {
        dbManager=[[DBManager alloc] initWithDatabaseName:@"MoreApps.sqlite"];
        selectQuery=0;
    }
    return self;
}
-(NSMutableArray *) listOfArchives{
    selectQuery=0;
    self.appsArray=nil;
    appsArray=[[NSMutableArray alloc] init];
    NSString *statement=[NSString stringWithFormat:@"SELECT APP_ID,APP_NAME_ENGLISH,APP_LINK,APP_DESCRIPTION_ENGLISH,APP_ICON,APP_NAME_ARABIC,APP_DESCRIPTION_ARABIC from MORE_APP;"];
    [dbManager selectOperation:self sqlStatementString:statement];
    return appsArray;
}
-(NSMutableArray *) archiveEnglishForId:(int) index{
    
    self.appsArray=nil;
    appsArray=[[NSMutableArray alloc] init];
    NSString *statement=[NSString stringWithFormat:@"SELECT * from MORE_APP WHERE APP_ID='%D';",index];
    [dbManager selectOperation:self sqlStatementString:statement];
    return appsArray;
}



#pragma mark SelectOperationDelegate functions

-(void) selectSuccessfullWithRow:(sqlite3_stmt*) resultRow
{
    
   
        MoreApp *app = [[MoreApp alloc] init];
            app.appID=[[NSNumber numberWithInt:sqlite3_column_int(resultRow,0) ]intValue];
        
        if (sqlite3_column_text(resultRow,1)!=NULL ) {
            app.appNameEnglish=[NSString stringWithUTF8String:(char*)sqlite3_column_text(resultRow,1)] ;
            
        }
        if (sqlite3_column_text(resultRow,2)!=NULL ) {
            app.appLink=[NSString stringWithUTF8String:(char*)sqlite3_column_text(resultRow,2)] ;
            
        }
        if (sqlite3_column_text(resultRow,3)!=NULL ) {
            app.appDescriptionEnglish=[NSString stringWithUTF8String:(char*)sqlite3_column_text(resultRow,3)];
            
        }
        if (sqlite3_column_text(resultRow,4)!=NULL ) {
            app.appIcon=[NSString stringWithUTF8String:(char*)sqlite3_column_text(resultRow,4)] ;
            
        }
    
    
        if (sqlite3_column_text(resultRow,5)!=NULL ) {
        app.appNameArabic=[NSString stringWithUTF8String:(char*)sqlite3_column_text(resultRow,5)] ;
        
        }
        if (sqlite3_column_text(resultRow,6)!=NULL ) {
        app.appDescriptionArabic=[NSString stringWithUTF8String:(char*)sqlite3_column_text(resultRow,6)] ;
        
        }
    
        [appsArray addObject:app];

     }


-(void) selectFailed
{
	NSLog(@"Select Failed");
}

@end
