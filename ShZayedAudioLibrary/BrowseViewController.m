//
//  BrowseViewController.m
//  Yomiyat
//
//  Created by Aisha on 5/20/13.
//  Copyright (c) 2013 Aisha. All rights reserved.
//

#import "BrowseViewController.h"
//#import "YoumiyatsListsViewController.h"
//#import "BrowseByDateViewController.h"
#import "Common.h"
#import "CustomCellHome.h"
//#import "TextViewViewController.h"
#import "MyUtilities.h"

#import "MFSideMenu.h"
#import "SideMenuViewController.h"
#import "UINavigationController+MFSideMenu.h"
#import "UIView+MWParallax.h"
#import "AdvancedViewController.h"
#import "MoreAppsViewController.h"
#define  REDCOLOR [[UIColor alloc] initWithRed:0.43 green:0.14 blue:0.17 alpha:1]
@interface BrowseViewController ()

@property(nonatomic,strong) UIImage *placeholder;

@end

@implementation BrowseViewController

#pragma mark - Parser Delegate

- (void)processCompleted{
    
}
- (void)processHasErrors
{
    UIAlertView *alertView=[[UIAlertView alloc] initWithTitle:@"Connectivity failed" message:@"No Gallery fetched from server" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
    [alertView show];
}




#pragma mark - View Lifecycle

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {

           }
    return self;
}
- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation duration:(NSTimeInterval)duration
{
    
}



- (IBAction)dismissKeyPad:(id)sender {
    [searchTextField resignFirstResponder];
}



-(void) viewWillDisappear:(BOOL)animated
{
    [audioPlayer stop];
    audioPlayer=nil;
}
-(void) viewDidDisappear:(BOOL)animated
{
    [audioPlayer stop];
    audioPlayer=nil;
}
-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
//    if (IS_IPHONE) {
//        if([[UIScreen mainScreen] bounds].size.height > 480)
//        {
//            homeImageView.iOS6ParallaxIntensity = 5;
//            tableView.iOS6ParallaxIntensity = 5;
//            mpVolumeViewParentView.iOS6ParallaxIntensity = 5;
//            playPauseButton.iOS6ParallaxIntensity = 5;
//            playerSlider.iOS6ParallaxIntensity = 5;
//            helpScreen.iOS6ParallaxIntensity = 5;
//
//        }
//        
//    }
//    if (IS_IPAD) {
//        homeImageView.iOS6ParallaxIntensity = 15;
//        tableView.iOS6ParallaxIntensity = 15;
//        mpVolumeViewParentView.iOS6ParallaxIntensity = 15;
//        playPauseButton.iOS6ParallaxIntensity = 15;
//        playerSlider.iOS6ParallaxIntensity = 15;
//        helpScreen.iOS6ParallaxIntensity = 15;
//
//    }
    playPauseButton.enabled = NO;
    selectedIndexPath = -1;
    if (IS_IPAD) {
        if (searchArray.count<=0) {
            toYearDropDownButton.enabled=NO;
            fromYearDropDownButton.enabled=NO;
            categoryDropDownButton.enabled=NO;
            playAllButton.enabled = NO;
            // placeDropDownButton.enabled=NO;
        }
        else{
            toYearDropDownButton.enabled=YES;
            fromYearDropDownButton.enabled=YES;
            categoryDropDownButton.enabled=YES;
            if (searchArray.count>0) {
                playAllButton.enabled = YES;
                
            }            // placeDropDownButton.enabled=YES;
        }
        
        if(IS_IPAD)
        {
            
            titleLabel.text = [self languageSelectedStringForKeyArabic:@"Photo Archives"];
            
            // [placeDropDownButton setBackgroundImage:[UIImage imageNamed:@"filter A.png"] forState:UIControlStateNormal];
            [categoryDropDownButton setBackgroundImage:[UIImage imageNamed:@"iPhone5 Larg filter.png"] forState:UIControlStateNormal];
            [fromYearDropDownButton setBackgroundImage:[UIImage imageNamed:@"iPhone5 Year filter.png"] forState:UIControlStateNormal];
            [toYearDropDownButton setBackgroundImage:[UIImage imageNamed:@"iPhone5 Year filter.png"] forState:UIControlStateNormal];
            
        }
        else{
            
            
            // [placeDropDownButton setBackgroundImage:[UIImage imageNamed:@"filter A.png"] forState:UIControlStateNormal];
            [categoryDropDownButton setBackgroundImage:[UIImage imageNamed:@"iPhone5 Category filter.png"] forState:UIControlStateNormal];
            [fromYearDropDownButton setBackgroundImage:[UIImage imageNamed:@"iPhone5 Year filter.png"] forState:UIControlStateNormal];
            [toYearDropDownButton setBackgroundImage:[UIImage imageNamed:@"iPhone5 Year filter.png"] forState:UIControlStateNormal];
            
            
        }
        
        
        
        
        [fromYearDropDownButton setTitle:[self languageSelectedStringForKeyArabic:@"Start Date"] forState:UIControlStateNormal];
        [toYearDropDownButton setTitle:[self languageSelectedStringForKeyArabic:@"End Date"] forState:UIControlStateNormal];
        //[placeDropDownButton setTitle:[self languageSelectedStringForKeyArabic:@"Location"] forState:UIControlStateNormal];
        [categoryDropDownButton setTitle:[self languageSelectedStringForKeyArabic:@"Category"] forState:UIControlStateNormal];
        
        UIImage *image=[UIImage imageNamed:@"sub-menu larg 1.png"];
        UIImage *image2=[UIImage imageNamed:@"sub-menu small 1.png"];
        [categoryDropDownButton initButtonWithData:categoriesDropDownArray parentTarget:self toView:self.view cellBackground:image];
        //  [placeDropDownButton initButtonWithData:placesDropdownArray parentTarget:self toView:self.view cellBackground:image];
        [toYearDropDownButton initButtonWithData:datesDropDownArray parentTarget:self toView:self.view cellBackground:image2];
        [fromYearDropDownButton initButtonWithData:datesDropDownArray parentTarget:self toView:self.view cellBackground:image2];
        
//        [self makeFilterComponentsForSearch];
        

    }
       self.navigationController.navigationBarHidden=YES;
    NSString *soundFilePath = [[NSBundle mainBundle] pathForResource:@"Track 1"
                                                              ofType:@"mp3"];
    NSURL *soundFileURL = [NSURL fileURLWithPath:soundFilePath];
    
    if (audioPlayer) {
        [audioPlayer stop];
        audioPlayer=nil;
    }
    
    audioPlayer = [[AVAudioPlayer alloc]initWithContentsOfURL:soundFileURL error:nil ];// initWithContentsOfURL:soundFileURL error:nil];    audioPlayer.numberOfLoops = 0;
    [audioPlayer prepareToPlay];
    [self openSearchResult:nil];
    if (IS_IPAD) {
        if (searchArray.count<=0) {
            toYearDropDownButton.enabled=NO;
            fromYearDropDownButton.enabled=NO;
            categoryDropDownButton.enabled=NO;
            playAllButton.enabled = NO;
            // placeDropDownButton.enabled=NO;
        }
        else{
            toYearDropDownButton.enabled=YES;
            fromYearDropDownButton.enabled=YES;
            categoryDropDownButton.enabled=YES;
            if (searchArray.count>0) {
                playAllButton.enabled = YES;
                
            }            // placeDropDownButton.enabled=YES;
        }
    }
   // [self performSelector:@selector(startLoadingData) withObject:nil afterDelay:0.5];

}
-(IBAction)advancedButtonPressed:(id)sender{
    AdvancedViewController *searchResultsViewController = [[AdvancedViewController alloc] initWithNibName:@"AdvancedViewController" bundle:nil];
  
    [self.navigationController pushViewController:searchResultsViewController animated:YES];
}
-(NSString*) languageSelectedStringForKeyArabic:(NSString*) key
{
    // [self test];
    int  selectedLanguage=2;
	NSString *path;
	if(selectedLanguage==1)
		path = [[NSBundle mainBundle] pathForResource:@"English" ofType:@"lproj"];
	else if(selectedLanguage==2)
		path = [[NSBundle mainBundle] pathForResource:@"zh" ofType:@"lproj"];
    
	NSBundle* languageBundle = [NSBundle bundleWithPath:path];
	NSString* str=[languageBundle localizedStringForKey:key value:@"" table:nil];
	return str;
}

-(NSString*) languageSelectedStringForKey:(NSString*) key
{
    // [self test];
    int  selectedLanguage=1;
	NSString *path;
	if(selectedLanguage==1)
		path = [[NSBundle mainBundle] pathForResource:@"English" ofType:@"lproj"];
	else if(selectedLanguage==2)
		path = [[NSBundle mainBundle] pathForResource:@"zh" ofType:@"lproj"];
    
	NSBundle* languageBundle = [NSBundle bundleWithPath:path];
	NSString* str=[languageBundle localizedStringForKey:key value:@"" table:nil];
	return str;
}
-(void)enteredBackground:(NSNotification *)notification {
    [audioPlayer stop];
    audioPlayer=nil;
    playPauseButton.enabled = NO;
    selectedIndexPath = -1;
    [browseTableView reloadData];

}
- (void)viewDidLoad
{
   [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(enteredBackground:)
                                                 name:UIApplicationDidEnterBackgroundNotification
                                               object:nil];
    self.navigationController.navigationBar.topItem.title = @"العودة";

    browseTableView.backgroundColor = [UIColor clearColor];
    languageSharedManager = [LanguageManager sharedManager];
    languageChange = 1;
    archivesDB=[[PhotoArchivesDataManager alloc] init];
    dropDownViewArray=[[NSMutableArray alloc] init];
    searchArray = [[NSMutableArray alloc] init];

        mpVolumeViewParentView.backgroundColor = [UIColor clearColor];
    MPVolumeView *myVolumeView =[[MPVolumeView alloc] initWithFrame: mpVolumeViewParentView.bounds];
    [mpVolumeViewParentView addSubview: myVolumeView];
//   [self setView];
    
    
    
    self.placeholder = [UIImage imageNamed:@"placeholder.png"];

}

#pragma mark - AUdio Player

- (void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag{
    if (playAll) {
        [activityIndicator startAnimating];

        isPause = NO;
        selectedIndexPath = selectedIndexPath+1;
        AudioArchive *photoArchive =[searchArray objectAtIndex:selectedIndexPath];
        
        
        NSURL *soundFileURL = [NSURL fileURLWithPath:[photoArchive.audioFile stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];

        NSData *_objectData = [NSData dataWithContentsOfURL:[NSURL URLWithString: [NSString stringWithFormat:@"http://apps.na.ae/ncdr/ncdr-app/NCDRApp/News/AudioLibrary/%@",photoArchive.audioFile ]]];
        NSError *error;
        
        audioPlayer = [[AVAudioPlayer alloc]initWithData:_objectData error:&error ];// initWithContentsOfURL:soundFileURL error:nil];
        audioPlayer.delegate = self;
        audioPlayer.numberOfLoops = 0;
        
        playerSlider.maximumValue = [audioPlayer duration];
        playerSlider.value = 0.0;
        
        [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updateTime:) userInfo:nil repeats:YES];
        [audioPlayer play];
        playPauseButton.enabled = YES;
        [playPauseButton setImage:[UIImage imageNamed:@"pause.png"] forState:UIControlStateNormal];
        [browseTableView reloadData];
        [activityIndicator stopAnimating];

    }

}
- (IBAction)playPausePressed:(id)sender {
    
  //  UIButton *btn = (UIButton *)sender;
    
        if (audioPlayer.isPlaying) {
        [audioPlayer pause];
         [playPauseButton setImage:[UIImage imageNamed:@"play.png"] forState:UIControlStateNormal];
        isPause = YES;

    }
    else{
        [audioPlayer play];
         [playPauseButton setImage:[UIImage imageNamed:@"pause.png"] forState:UIControlStateNormal];
        isPause = NO;

    }
    [browseTableView reloadData];
}
- (IBAction)playAllPressed:(id)sender {
    if (searchArray.count>0) {

    playAll = YES;
 //   [activityIndicator startAnimating];

    if (audioPlayer.isPlaying) {
        [audioPlayer stop];
    }
    isPause = NO;
    selectedIndexPath = 0;
    AudioArchive *photoArchive =[searchArray objectAtIndex:selectedIndexPath];

    NSData *_objectData = [NSData dataWithContentsOfURL:[NSURL URLWithString: [NSString stringWithFormat:@"http://apps.na.ae/ncdr/ncdr-app/NCDRApp/News/AudioLibrary/%@",photoArchive.audioFile ]]];
    NSError *error;
        
    
    audioPlayer = [[AVAudioPlayer alloc]initWithData:_objectData error:&error ];// initWithContentsOfURL:soundFileURL error:nil];
    audioPlayer.delegate = self;
    audioPlayer.numberOfLoops = 0;
    playerSlider.maximumValue = [audioPlayer duration];
    playerSlider.value = 0.0;
    
    [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updateTime:) userInfo:nil repeats:YES];
    playAll = YES;

    [audioPlayer play];
    playPauseButton.enabled = YES;
    [playPauseButton setImage:[UIImage imageNamed:@"pause.png"] forState:UIControlStateNormal];
    [browseTableView reloadData];
    }
    
}
- (IBAction)introButtonPressed:(id)sender {
}
- (IBAction)helpButtonPressed:(id)sender {
    if (IS_IPAD) {
        [helpScreen setBackgroundImage:[UIImage imageNamed:@"iPad home page help.png"] forState:UIControlStateNormal];

    }
    else{
        [helpScreen setBackgroundImage:[UIImage imageNamed:@"iPhone5 home page help.png"] forState:UIControlStateNormal];

    }
    helpScreen.hidden = NO;
    
}
- (IBAction)dismissHelpPressed:(id)sender {
    helpScreen.hidden = YES;
}
- (IBAction)moreButtonPressed:(id)sender {
    if (IS_IPAD) {
        MoreAppsViewController *searchResultsViewController = [[MoreAppsViewController alloc] initWithNibName:@"MoreAppsViewControlleriPad" bundle:nil];
        
        [self.navigationController pushViewController:searchResultsViewController animated:YES];
    }
    else{
        MoreAppsViewController *searchResultsViewController = [[MoreAppsViewController alloc] initWithNibName:@"MoreAppsViewController" bundle:nil];

        [self.navigationController pushViewController:searchResultsViewController animated:YES];
    }
   
}
- (void)presentAppStoreForID:(NSNumber *)appStoreID withDelegate:(id<SKStoreProductViewControllerDelegate>)delegate withURL:(NSURL *)appStoreURL
{
    if(NSClassFromString(@"SKStoreProductViewController")) { // Checks for iOS 6 feature.
        
        SKStoreProductViewController *storeController = [[SKStoreProductViewController alloc] init];
        storeController.delegate = delegate; // productViewControllerDidFinish
        
        // Example app_store_id (e.g. for Words With Friends)
        // [NSNumber numberWithInt:322852954];
        
        NSDictionary *productParameters = @{ SKStoreProductParameterITunesItemIdentifier : appStoreID };
        
        
        [storeController loadProductWithParameters:productParameters completionBlock:^(BOOL result, NSError *error) {
            if (result) {
                [self presentViewController:storeController animated:YES completion:nil];
            } else {
                [[[UIAlertView alloc] initWithTitle:@"Uh oh!" message:@"There was a problem displaying the app" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil] show];
            }
        }];
        
        
    } else { // Before iOS 6, we can only open the URL
        [[UIApplication sharedApplication] openURL:appStoreURL];
    }
}
- (void)productViewControllerDidFinish:(SKStoreProductViewController *)viewController
{
    [viewController dismissViewControllerAnimated:YES completion:nil];
    
}

-(void) startLoadingData
{

}

-(void) setView
{

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    [audioPlayer stop];
    audioPlayer=nil;
    // Dispose of any resources that can be recreated.
}

-(IBAction)openSearchResult:(id)sender
{
    
        //if(searchTextField.text.length>0)
        //{
            [searchTextField resignFirstResponder];
            [self makeResultsFromFiltersAndDatesSelected];
            
        //}
    
}
#pragma mark UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    if (searchTextField == textField) {
    
        [textField resignFirstResponder];
        
        toYearDropDownButton.enabled=YES;
        fromYearDropDownButton.enabled=YES;
        categoryDropDownButton.enabled=YES;
        
        if (searchArray.count>0) {
        
            playAllButton.enabled = YES;

        }
        
        [self openSearchResult:nil];
        return NO;
    }
    
    return YES;
}
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
//    if ([textField.text isEqualToString:@""]) {
//        toYearDropDownButton.enabled=NO;
//        fromYearDropDownButton.enabled=NO;
//        categoryDropDownButton.enabled=NO;
//        if (searchArray.count>0) {
//            playAllButton.enabled = NO;
//            
//        }       // placeDropDownButton.enabled=NO;
//    }
//    else{
//        toYearDropDownButton.enabled=YES;
//        fromYearDropDownButton.enabled=YES;
//        categoryDropDownButton.enabled=YES;
//        if (searchArray.count>0) {
//            playAllButton.enabled = YES;
//            
//        }      //  placeDropDownButton.enabled=YES;
//    }
    [self openSearchResult:nil];

    return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    [self openSearchResult:nil];

    
    return YES;
}
#pragma mark touches and gesture events

-(void)addGesture
{
   
    UISwipeGestureRecognizer *swipeGestureR = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeGestureRight:)];
    [swipeGestureR setDirection: UISwipeGestureRecognizerDirectionRight ];
    [self.view addGestureRecognizer:swipeGestureR];
    
    UISwipeGestureRecognizer *swipeGestureL = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeGestureLeft:)];
    [swipeGestureL setDirection: UISwipeGestureRecognizerDirectionLeft];
    [self.view addGestureRecognizer:swipeGestureL];
   
    if(IS_IPAD) {
    
        UIPinchGestureRecognizer *pinch=[[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(onDoubleTapTextView:)];
        [self.view addGestureRecognizer:pinch];
    }
}


-(void) handleSwipeGestureRight:(UISwipeGestureRecognizer *) sender
{
    NSLog(@"right %i",currentIndex);
    
    if(currentIndex<([searchArray count]-1))
    {
        currentIndex++;
    }
    
}

-(void) handleSwipeGestureLeft:(UISwipeGestureRecognizer *) sender
{
    NSLog(@"left %i",currentIndex);
    
    if(currentIndex>0)
    {
        currentIndex--;
    }
   
    
}

-(IBAction)APlus:(id)sender
{
    if(fontSize<40)
    {
        fontSize++;
    }
}

-(IBAction)AMinus:(id)sender
{
    if(fontSize>20)
    {
        fontSize--;
    }
}

-(void)onDoubleTapTextView:(id)sender
{
    UIPinchGestureRecognizer *pinch=(UIPinchGestureRecognizer*)sender;
    if(pinch.scale>1)
        [self APlus:sender];
    else
        [self AMinus:sender];
}


-(void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [DropDownButton dismissButtons:dropDownViewArray callingButton:nil];
}

-(void)goBack:(id) sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

-(IBAction) goBackToList:(id)sender
{
    browseTableView.hidden=NO;
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
//#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
//    NSLog(@"Search Array Count %@",searchArray);
//#warning Incomplete method implementation.
    // Return the number of rows in the section.
    return [searchArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
//    static NSString *CellIdentifier = @"Cell";
    NSString *CellIdentifier = [NSString stringWithFormat:@"Cell_%ld_%ld",indexPath.row,indexPath.section];
    CustomCellHome *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
//    CustomCellHome *cell = [[CustomCellHome alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[CustomCellHome alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
       AudioArchive *photoArchive =[searchArray objectAtIndex:indexPath.row];
    [cell setTag:indexPath.row];
    
    cell.backgroundColor = [UIColor clearColor];
//    NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:
//                                                       [NSString stringWithFormat:@"http://apps.na.ae/ncdr/ncdr-app/NCDRApp/News/AudioLibrary/%@", photoArchive.iconFile]]];
    
    cell.imageView.image = [UIImage imageNamed:@"placeholder.png"];
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
    dispatch_async(queue, ^(void) {
        
        NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://apps.na.ae/ncdr/ncdr-app/NCDRApp/News/AudioLibrary/%@", photoArchive.iconFile]]];
                             
                             UIImage* image = [[UIImage alloc] initWithData:imageData];
                             if (image) {
                                 dispatch_async(dispatch_get_main_queue(), ^{
                                     if (cell.tag == indexPath.row) {
                                         cell.imageView.image = image;
                                         [cell setNeedsLayout];
                                     }
                                 });
                             }
                             });
                             
//    if (imageData == nil) {
//        cell.imageView.image  =[UIImage imageWithData:imageData];//[UIImage imageNamed:[NSString stringWithFormat:@"%@",photoArchive.relatedPhoto]];// [UIImage imageWithData:imageData];
//    }else {
//        cell.imageView.image = [UIImage imageNamed:@"placeholder.png"];
//    }
    
    

    cell.backgroundView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"row_n.png"]];
    cell.selectedBackgroundView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"selection pause.png"]];

    if (languageChange==0) {//english
        cell.detailTextLabel.text=photoArchive.audioTitle;
        cell.textLabel.text=[NSString stringWithFormat:@"Category:%@",photoArchive.audioSubject] ;
        
        cell.detailTextLabel.textAlignment=UITextAlignmentLeft;
        cell.textLabel.textAlignment=UITextAlignmentLeft;
        cell.textLabel.numberOfLines=1;
        cell.detailTextLabel.numberOfLines=1;
        cell.textLabel.font=[UIFont fontWithName:@"Ariel" size:11];
        cell.detailTextLabel.font=[UIFont fontWithName:@"Ariel" size:11];
        cell.detailTextLabel.lineBreakMode = UILineBreakModeTailTruncation;
        cell.textLabel.lineBreakMode = UILineBreakModeTailTruncation;
    }
    else{//arabic
        cell.detailTextLabel.text=photoArchive.audioTitle;
//        NSString *soundFilePath = [[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"%@",photoArchive.audioFile ]
//                                                                  ofType:@"mp3"];
//        NSURL *soundFileURL = [NSURL fileURLWithPath:soundFilePath];
        NSURL *soundFileURL = [NSURL fileURLWithPath:[photoArchive.audioFile stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
        NSData *_objectData = [NSData dataWithContentsOfURL:soundFileURL];
        NSError *error;
        
      // audioPlayer = [[AVAudioPlayer alloc]initWithData:_objectData error:&error ];// initWithContentsOfURL:soundFileURL error:nil];
        
        AVURLAsset* audioAsset = [AVURLAsset URLAssetWithURL:soundFileURL options:nil];
        CMTime audioDuration = audioAsset.duration;
        float audioDurationSeconds = CMTimeGetSeconds(audioDuration);
        
        cell.detailTextLabel.text=photoArchive.audioTitle;
        cell.textLabel.text=[NSString stringWithFormat:@"الموضوع:%@",photoArchive.audioSubject];

        
        cell.detailTextLabel.textAlignment=UITextAlignmentRight;
        cell.textLabel.textAlignment=UITextAlignmentRight;
        cell.textLabel.numberOfLines=1;
        cell.detailTextLabel.numberOfLines=1;
        cell.detailTextLabel.lineBreakMode = UILineBreakModeTailTruncation;
        cell.textLabel.lineBreakMode = UILineBreakModeTailTruncation;
        
        if (IS_IPHONE) {
            cell.textLabel.textColor = [UIColor whiteColor];
            cell.detailTextLabel.textColor = [UIColor whiteColor];

        }
     
    }
    if (indexPath.row==selectedIndexPath) {
       // [cell setHighlighted:YES];
        if (!isPause) {
            cell.backgroundView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"selection pause.png"]];

        }
        else{
            cell.backgroundView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"selection play.png"]];

        }
    }
    else {
       // [cell setHighlighted:NO];
    }
    
    return cell;
}

- (void) playSelected:(id) sender;
{
    NSLog(@"Play song number %d", [sender tag]);
    AudioArchive *photoArchive =[searchArray objectAtIndex:[sender tag]];

//    NSString *soundFilePath = [[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"%@",photoArchive.audioFile ]
//                                                              ofType:@"mp3"];
//    NSURL *soundFileURL = [NSURL fileURLWithPath:soundFilePath];
    NSURL *soundFileURL = [NSURL fileURLWithPath:[photoArchive.audioFile stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];

    NSData *_objectData = [NSData dataWithContentsOfURL:[NSURL URLWithString: [NSString stringWithFormat:@"http://apps.na.ae/ncdr/ncdr-app/NCDRApp/News/AudioLibrary/%@",photoArchive.audioFile ]]];
    NSError *error;
    
    audioPlayer = [[AVAudioPlayer alloc]initWithData:_objectData error:&error ];// initWithContentsOfURL:soundFileURL error:nil];
    
    audioPlayer.delegate = self;
    audioPlayer.numberOfLoops = 0;
    
    playerSlider.maximumValue = [audioPlayer duration];
    playerSlider.value = 0.0;
    
    [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updateTime:) userInfo:nil repeats:YES];
    [audioPlayer play];

}

- (IBAction)slide {
    audioPlayer.currentTime = playerSlider.value;
}

- (void)updateTime:(NSTimer *)timer {
    playerSlider.value = audioPlayer.currentTime;
}
#pragma mark - Table view delegate


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    playAll=NO;
  //  [activityIndicator startAnimating];
    
    if (indexPath.row==selectedIndexPath) {
        playPauseButton.enabled = YES;

        if (isPause) {
            [audioPlayer play];
            [playPauseButton setImage:[UIImage imageNamed:@"pause.png"] forState:UIControlStateNormal];
            isPause=NO;

        }
        else{
            [audioPlayer pause];
            isPause=YES;
            [playPauseButton setImage:[UIImage imageNamed:@"play.png"] forState:UIControlStateNormal];

        }


    }
    else{
        if (audioPlayer.isPlaying) {
            [audioPlayer stop];
        }
        selectedIndexPath = indexPath.row;
        isPause = NO;
        AudioArchive *photoArchive =[searchArray objectAtIndex:indexPath.row];
        //http://apps.na.ae/ncdr/ncdr-app/NCDRApp/AudioLibrary/Track%201.mp3
   //     NSString *soundFilePath = [[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"%@",photoArchive.audioFile ] ofType:@"mp3"];
       // NSURL *soundFileURL = [NSURL fileURLWithPath:[photoArchive.audioFile stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
        NSURL *soundFileURL = [NSURL URLWithString:[photoArchive.audioFile stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
      //  http://apps.na.ae/ncdr/ncdr-app/NCDRApp/News/Track_1.mp3
//http://songs.apniisp.com/Strings%20-%20Dhaani/03%20-%20Kahani%20Mohabat%20Ki%20(Apniisp.Com).mp3
   
        NSData *_objectData = [NSData dataWithContentsOfURL:[NSURL URLWithString: [NSString stringWithFormat:@"http://apps.na.ae/ncdr/ncdr-app/NCDRApp/News/AudioLibrary/%@",photoArchive.audioFile ]]];
        NSError *error;

        audioPlayer = [[AVAudioPlayer alloc]initWithData:_objectData error:&error ];// initWithContentsOfURL:soundFileURL error:nil];
        audioPlayer.delegate = self;
        audioPlayer.numberOfLoops = 0;
        if (audioPlayer == nil)
            NSLog([error description]);
        playerSlider.maximumValue = [audioPlayer duration];
        playerSlider.value = 0.0;
        
        [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updateTime:) userInfo:nil repeats:YES];
        [audioPlayer play];
        playPauseButton.enabled = YES;
        [playPauseButton setImage:[UIImage imageNamed:@"pause.png"] forState:UIControlStateNormal];

    }
    
    [tableView reloadData];
   // [activityIndicator stopAnimating];

}


#pragma mark browse method
-(void) makeFilterComponentsForSearch
{
    
    UIImage *image=[UIImage imageNamed:@"sub-menu larg 1.png"];
    UIImage *image2=[UIImage imageNamed:@"sub-menu small 1.png"];

    
        ////place///
//        NSMutableSet *placesSet = [[NSMutableSet alloc]init];
//        for (NSString *places in [archivesDB listofPhotoPlacesArabic]) {
//            NSArray *items = [places componentsSeparatedByString:@","];
//            for (NSString *place in items) {
//                [placesSet addObject:place];
//            }
//        }
//        placesDropdownArray = [[NSMutableArray alloc] initWithArray:[placesSet allObjects]];
//        [placesDropdownArray insertObject:@"الكل" atIndex:0];
//        [placeDropDownButton initButtonWithData:placesDropdownArray parentTarget:self toView:self.view cellBackground:image];
//        
        ////category///
        NSMutableSet *categoriesSet = [[NSMutableSet alloc]init];
        for (NSString *categories in [archivesDB listofPhotoCategoriesArabic]) {
            NSArray *items = [categories componentsSeparatedByString:@","];
            for (NSString *category in items) {
                [categoriesSet addObject:category];
            }
        }
        categoriesDropDownArray = [[NSMutableArray alloc] initWithArray:[categoriesSet allObjects]];
        [categoriesDropDownArray insertObject:@"الكل" atIndex:0];
        [categoryDropDownButton initButtonWithData:categoriesDropDownArray parentTarget:self toView:self.view cellBackground:image];
        
        ////dateTo///
        datesDropDownArray = [[NSMutableArray alloc]init];
//        NSMutableSet *datesSet = [[NSMutableSet alloc]init];
//        
//        for (NSDictionary *dic in [archivesDB listofPhotoDates]) {
//            [datesSet addObject:[dic objectForKey:@"YearFrom"]];
//            //[datesSet addObject:[dic objectForKey:@"YearTo"]];
//            
//        }
        NSArray *sortedArray = [[archivesDB listofPhotoDates] sortedArrayUsingComparator: ^(id firstString, id secondString) {
            return [(NSString *)secondString compare:(NSString *)firstString  options:NSNumericSearch];
        }];
        datesDropDownArray = [[NSMutableArray alloc] initWithArray: sortedArray];

        [datesDropDownArray insertObject:@"الكل" atIndex:0];
        
        [fromYearDropDownButton initButtonWithData:datesDropDownArray parentTarget:self toView:self.view cellBackground:image2];
        [toYearDropDownButton initButtonWithData:datesDropDownArray parentTarget:self toView:self.view cellBackground:image2];
    
   

      //////////////////////////////////////////////////////////////////////////////////////////

   // [dropDownViewArray addObject:placeDropDownButton];
    [dropDownViewArray addObject:categoryDropDownButton];
    [dropDownViewArray addObject:fromYearDropDownButton];
    [dropDownViewArray addObject:toYearDropDownButton];
    
}
//NSInteger ticketSort(id ticket1, id ticket2, void *context) {
//	return [ticket2 compare:ticket1 options:NSNumericSearch];
//}
#pragma mark MENU PROTOCOL

-(void) indexClickedForDropDownMenuIndex:(int) index andDropDown:(id)dropDown;
{
    [self goBackToList:nil];
    
    
   
    if(dropDown==categoryDropDownButton)
    {
        if(IS_IPAD)
        {
            if (languageChange==0) {
                [categoryDropDownButton setBackgroundImage:[UIImage imageNamed:@"iPhone5 Larg filter.png"] forState:UIControlStateNormal];
                
            }
            else{
                [categoryDropDownButton setBackgroundImage:[UIImage imageNamed:@"iPhone5 Larg filter.png"] forState:UIControlStateNormal];
                
            }
        }
        else
        {
            if (languageChange==0) {
                [categoryDropDownButton setBackgroundImage:[UIImage imageNamed:@"iPhone5 Larg filter.png"] forState:UIControlStateNormal];
                
            }
            else{
                [categoryDropDownButton setBackgroundImage:[UIImage imageNamed:@"iPhone5 Larg filter.png"] forState:UIControlStateNormal];
                
            }        }
        if (languageChange==0) {
            if ([[categoriesDropDownArray objectAtIndex:index] isEqualToString:[self languageSelectedStringForKey:@"ALL"]]||[[categoriesDropDownArray objectAtIndex:index] isEqualToString:[self languageSelectedStringForKey:@"الكل"]]) {
                [categoryDropDownButton setTitle:[self languageSelectedStringForKey:@"Category"] forState:UIControlStateNormal];
                
            }
            else{
                [categoryDropDownButton setTitle:[categoriesDropDownArray objectAtIndex:index] forState:UIControlStateNormal];
            }
        }
        else{
            if ([[categoriesDropDownArray objectAtIndex:index] isEqualToString:[self languageSelectedStringForKey:@"ALL"]]||[[categoriesDropDownArray objectAtIndex:index] isEqualToString:[self languageSelectedStringForKey:@"الكل"]]) {
                [categoryDropDownButton setTitle:[self languageSelectedStringForKey:@"الموضوع"] forState:UIControlStateNormal];
                
            }
            else{
                [categoryDropDownButton setTitle:[categoriesDropDownArray objectAtIndex:index] forState:UIControlStateNormal];
            }
        }
        
    }
    if(dropDown==fromYearDropDownButton)
    {
        if(IS_IPAD)
        {
            if (languageChange==0) {
                [fromYearDropDownButton setBackgroundImage:[UIImage imageNamed:@"iPhone5 Year filter.png"] forState:UIControlStateNormal];
                
            }
            else{
                [fromYearDropDownButton setBackgroundImage:[UIImage imageNamed:@"iPhone5 Year filter.png"] forState:UIControlStateNormal];
                
            }
        }
        else
        {
            if (languageChange==0) {
                [fromYearDropDownButton setBackgroundImage:[UIImage imageNamed:@"iPhone5 Year filter.png"] forState:UIControlStateNormal];
                
            }
            else{
                [fromYearDropDownButton setBackgroundImage:[UIImage imageNamed:@"iPhone5 Year filter.png"] forState:UIControlStateNormal];
                
            }        }
        if (languageChange==0) {
            if ([[datesDropDownArray objectAtIndex:index] isEqualToString:[self languageSelectedStringForKey:@"ALL"]]||[[datesDropDownArray objectAtIndex:index] isEqualToString:[self languageSelectedStringForKey:@"الكل"]]) {
                [fromYearDropDownButton setTitle:[self languageSelectedStringForKey:@"Start Date"] forState:UIControlStateNormal];
                
                
            }
            else{
                [fromYearDropDownButton setTitle:[datesDropDownArray objectAtIndex:index] forState:UIControlStateNormal];
            }
        }
        else{
            if ([[datesDropDownArray objectAtIndex:index] isEqualToString:[self languageSelectedStringForKey:@"ALL"]]||[[datesDropDownArray objectAtIndex:index] isEqualToString:[self languageSelectedStringForKey:@"الكل"]]) {
                [fromYearDropDownButton setTitle:[self languageSelectedStringForKey:@"من عام"] forState:UIControlStateNormal];
                
                
            }
            else{
                [fromYearDropDownButton setTitle:[datesDropDownArray objectAtIndex:index] forState:UIControlStateNormal];
            }
        }
    }
    if(dropDown==toYearDropDownButton)
    {
        if(IS_IPAD)
        {
            if (languageChange==0) {
                [toYearDropDownButton setBackgroundImage:[UIImage imageNamed:@"iPhone5 Year filter.png"] forState:UIControlStateNormal];
                
            }
            else{
                [toYearDropDownButton setBackgroundImage:[UIImage imageNamed:@"iPhone5 Year filter.png"] forState:UIControlStateNormal];
                
            }        }
        else
        {
            if (languageChange==0) {
                [toYearDropDownButton setBackgroundImage:[UIImage imageNamed:@"iPhone5 Year filter.png"] forState:UIControlStateNormal];
                
            }
            else{
                [toYearDropDownButton setBackgroundImage:[UIImage imageNamed:@"iPhone5 Year filter.png"] forState:UIControlStateNormal];
                
            }        }
       
        
        if (languageChange==0) {
            if ([[datesDropDownArray objectAtIndex:index] isEqualToString:[self languageSelectedStringForKey:@"ALL"]]||[[datesDropDownArray objectAtIndex:index] isEqualToString:[self languageSelectedStringForKey:@"الكل"]]) {
                [toYearDropDownButton setTitle:[self languageSelectedStringForKey:@"End Date"] forState:UIControlStateNormal];
                
                
            }
            else{
                [toYearDropDownButton setTitle:[datesDropDownArray objectAtIndex:index] forState:UIControlStateNormal];
            }
        }
        else{
            if ([[datesDropDownArray objectAtIndex:index] isEqualToString:[self languageSelectedStringForKey:@"ALL"]]||[[datesDropDownArray objectAtIndex:index] isEqualToString:[self languageSelectedStringForKey:@"الكل"]]) {
//                [toYearDropDownButton setTitle:[self languageSelectedStringForKey:@"إلى عام"] forState:UIControlStateNormal];
                [toYearDropDownButton setTitle:[self languageSelectedStringForKeyArabic:@"End Date"] forState:UIControlStateNormal];
                
                
            }
            else{
                [toYearDropDownButton setTitle:[datesDropDownArray objectAtIndex:index] forState:UIControlStateNormal];
            }
        }
    }
    [self openSearchResult:nil];

}

#pragma mark rotation

- (BOOL)shouldAutorotate {
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}
// pre-iOS 6 support
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation {
    return (toInterfaceOrientation == UIInterfaceOrientationPortrait);
}

-(void)makeResultsFromFiltersAndDatesSelected
{
    

   // [self.view viewWithTag:101].hidden=NO;

    if (languageChange==0) {
        
//        searchArray= [archivesDB archivesEnglishFromDate:fromYearDropDownButton.titleLabel.text toDate:toYearDropDownButton.titleLabel.text inCategoryE:categoryDropDownButton.titleLabel.text withKeywordE:searchTextField.text atPlaceE:placeDropDownButton.titleLabel.text];
        
        searchArray = [archivesDB archivesArabicWithCategoryA:categoryDropDownButton.titleLabel.text withKeywordA:searchTextField.text toDate:toYearDropDownButton.titleLabel.text andFromDate:fromYearDropDownButton.titleLabel.text];

    } else {
//        searchArray= [archivesDB archivesArabicFromDate:fromYearDropDownButton.titleLabel.text toDate:toYearDropDownButton.titleLabel.text inCategoryA:categoryDropDownButton.titleLabel.text withKeywordA:searchTextField.text atPlaceA:placeDropDownButton.titleLabel.text ];
        
        searchArray = [archivesDB archivesArabicWithCategoryA:categoryDropDownButton.titleLabel.text withKeywordA:searchTextField.text toDate:toYearDropDownButton.titleLabel.text andFromDate:fromYearDropDownButton.titleLabel.text];


    }
   // [self.view viewWithTag:101].hidden=YES;

    if (searchArray.count>0) {
        playAllButton.enabled = YES;
        
    }
    if (IS_IPAD) {
        if (searchArray.count<=0) {
            toYearDropDownButton.enabled=YES;
            fromYearDropDownButton.enabled=YES;
            categoryDropDownButton.enabled=YES;
            playAllButton.enabled = NO;
            // placeDropDownButton.enabled=NO;
        }
        else{
            toYearDropDownButton.enabled=YES;
            fromYearDropDownButton.enabled=YES;
            categoryDropDownButton.enabled=YES;
            if (searchArray.count>0) {
                playAllButton.enabled = YES;
                
            }            // placeDropDownButton.enabled=YES;
        }
    }

    [browseTableView reloadData];
}





@end
