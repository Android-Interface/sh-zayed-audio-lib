//
//  MyUtilities.h
//  Yomiyat
//
//  Created by Aisha on 5/19/13.
//  Copyright (c) 2013 Aisha. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MyUtilities : NSObject

+(void) readHTMLFileName:(NSString*) fileName onView:(UIWebView*)webView;
+(void) removeshadowLineOnWebView:(UIWebView*)webView;
+(void) setSourceImagesOnForString:(NSString *) sourceName OnView:(UIImageView*) sourceImageView;


@end
